﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkLight.Editor.Controls
{
    /// <summary>
    /// Interaction logic for GamePanel.xaml
    /// </summary>
    public partial class GamePanel : UserControl
    {
        public static readonly DependencyProperty GameProperty = DependencyProperty.Register("Game", typeof(Game), typeof(GamePanel));

        public Game Game
        {
            get { return (Game)base.GetValue(GamePanel.GameProperty); }
            set { base.SetValue(GamePanel.GameProperty, value); }
        }

        public GamePanel()
        {
            this.InitializeComponent();
            base.Loaded += this.GamePanelLoaded;
        }

        private void GamePanelLoaded(object sender, RoutedEventArgs e)
        {
            if (this.Game == null)
            {
                return;
            }

            GameReflector.CreateGame(this.Game, _image);

            //Set the back buffer for the D3DImage, since unlocking it without one will thrown and exception
            this.SetD3DImageBackBuffer(this.CreateRenderTarget(1, 1));

            //Register for size changed if using a RenderTarget2D for drawing
            _image.SizeChanged += this.OnSizeChanged;
            //Register for Rendering to perform updates and drawing
            CompositionTarget.Rendering += this.OnRendering;
        }

        private void OnRendering(object sender, EventArgs e)
        {
            _d3DImage.Lock();
            //Update and draw the game, then invalidate the D3DImage
            this.Game.Tick();
            _d3DImage.AddDirtyRect(new Int32Rect(0, 0, _d3DImage.PixelWidth, _d3DImage.PixelHeight));
            _d3DImage.Unlock();

            Window.GetWindow(this).Title = this.Game.Window.Title;
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Can't create a RenderTarget2D with dimensions smaller than zero
            if ((int)_image.ActualWidth <= 0 || (int)_image.ActualHeight <= 0)
            {
                return;
            }

            RenderTarget2D renderTarget = this.CreateRenderTarget((int)_image.ActualWidth, (int)_image.ActualHeight);

            //Direct the game's drawing to the newly created RenderTarget2D,
            //whose surface will be displayed in the D3DImage
            this.Game.GraphicsDevice.SetRenderTarget(renderTarget);
            this.SetD3DImageBackBuffer(renderTarget);
        }

        private RenderTarget2D CreateRenderTarget(int width, int height)
        {
            return new RenderTarget2D(Game.GraphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.Depth24);
        }

        private void SetD3DImageBackBuffer(RenderTarget2D renderTarget)
        {
            _d3DImage.Lock();
            _d3DImage.SetBackBuffer(D3DResourceType.IDirect3DSurface9, GameReflector.GetRenderTargetSurface(renderTarget));
            _d3DImage.Unlock();
        }
    }
}
