﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

// http://blogs.msdn.com/b/nicgrave/archive/2011/03/25/wpf-hosting-for-xna-game-studio-4-0.aspx
// https://github.com/BinaryConstruct/BCCL/tree/master/BCCL.Xna/UI/Xaml/XnaContentHost

namespace Flai.Editor.Controls
{
    #region Native Methods

    internal static class NativeMethods
    {
        #region Constants

        // Define the window styles we use
        public const int WS_CHILD = 0x40000000;
        public const int WS_VISIBLE = 0x10000000;

        // Define the Windows messages we will handle
        public const int WM_MOUSEMOVE = 0x0200;
        public const int WM_MOUSELEAVE = 0x02A3;

        // Define the cursor icons we use
        public const int IDC_ARROW = 32512;

        // Define the TME_LEAVE value so we can register for WM_MOUSELEAVE messages
        public const uint TME_LEAVE = 0x00000002;

        #endregion

        #region Delegates and Structs

        public delegate IntPtr WndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        public static readonly WndProc DefaultWindowProc = DefWindowProc;

        [StructLayout(LayoutKind.Sequential)]
        public struct WNDCLASSEX
        {
            public uint cbSize;
            public uint style;
            [MarshalAs(UnmanagedType.FunctionPtr)]
            public WndProc lpfnWndProc;
            public int cbClsExtra;
            public int cbWndExtra;
            public IntPtr hInstance;
            public IntPtr hIcon;
            public IntPtr hCursor;
            public IntPtr hbrBackground;
            public string lpszMenuName;
            public string lpszClassName;
            public IntPtr hIconSm;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TRACKMOUSEEVENT
        {
            public int cbSize;
            public uint dwFlags;
            public IntPtr hWnd;
            public uint dwHoverTime;
        }

        #endregion

        #region DllImports

        [DllImport("user32.dll", EntryPoint = "CreateWindowEx", CharSet = CharSet.Auto)]
        public static extern IntPtr CreateWindowEx(
            int exStyle,
            string className,
            string windowName,
            int style,
            int x, int y,
            int width, int height,
            IntPtr hwndParent,
            IntPtr hMenu,
            IntPtr hInstance,
            [MarshalAs(UnmanagedType.AsAny)] object pvParam);

        [DllImport("user32.dll", EntryPoint = "DestroyWindow", CharSet = CharSet.Auto)]
        public static extern bool DestroyWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern IntPtr DefWindowProc(IntPtr hWnd, uint uMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetModuleHandle(string module);

        [DllImport("user32.dll")]
        public static extern IntPtr LoadCursor(IntPtr hInstance, int lpCursorName);

        [DllImport("user32.dll")]
        public static extern int TrackMouseEvent(ref TRACKMOUSEEVENT lpEventTrack);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.U2)]
        public static extern short RegisterClassEx([In] ref WNDCLASSEX lpwcx);

        [DllImport("user32.dll")]
        public static extern int SetFocus(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetFocus();

        #endregion
    }

    #endregion

    public abstract class GamePanelBase<T> : HwndHost
        where T : EditorGameBase
    {
        private const string WindowClassName = "GraphicsDeviceControlHostWindowClass";
        public static readonly DependencyProperty GameProperty = DependencyProperty.Register("Game", typeof(T), typeof(GamePanelBase<T>));

        // The HWND we present to when rendering
        private IntPtr _hWnd;
        private IntPtr _hWndPrev;

        private bool _handleSetToGame = false;

        public T Game
        {
            get { return (T)base.GetValue(GamePanelBase<T>.GameProperty); }
            set { base.SetValue(GamePanelBase<T>.GameProperty, value); }
        }

        protected GamePanelBase()
        {
            base.SizeChanged += this.PanelSizeChanged;

            Application.Current.Exit += this.OnApplicationExit;
            CompositionTarget.Rendering += this.OnCompositionTargetRendering;
        }

        private void PanelSizeChanged(object sender, SizeChangedEventArgs e)
        {
            // this.Game is null when this is called for the first time. This did bug did not exist in the original DarkLight editor, 
            // but exists in the Evolution. Possibly because new AvalonDock version? See also, "BuildWindowCore"
            if (this.Game != null)
            {
                // Okay and this throws some random exception in "UpdateResolution" even if this.Game is not null...
                // this.Game.Resolution = new Vector2i((int)e.NewSize.Width, (int)e.NewSize.Height);
            }
        }

        private void OnApplicationExit(object sender, ExitEventArgs e)
        {
            this.Game.DisposeIfNotNull();
        }

        private void OnCompositionTargetRendering(object sender, EventArgs e)
        {
            if (this.Game != null)
            {
                // See, "BuildWindowCore"
                if (!_handleSetToGame)
                {
                    this.Game.SetHandle(_hWnd);
                    _handleSetToGame = true;
                }

                // See, "PanelSizeChanged"
                this.Game.ChangeResolution((int)this.RenderSize.Width, (int)this.RenderSize.Height);
                this.Game.Tick();
            }
        }

        protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // This is required for scroll wheel to work in XNA
            // EDIT: Currently, the scrolling starts to work in xna when you press any mouse button within the XNA area
            if (msg == NativeMethods.WM_MOUSEMOVE)
            {
                if (this.Game.ShouldBeFocused)
                {
                    _hWndPrev = NativeMethods.GetFocus();
                    NativeMethods.SetFocus(_hWnd);
                }

                NativeMethods.TRACKMOUSEEVENT tme = new NativeMethods.TRACKMOUSEEVENT();
                tme.cbSize = Marshal.SizeOf(typeof(NativeMethods.TRACKMOUSEEVENT));
                tme.dwFlags = NativeMethods.TME_LEAVE;
                tme.hWnd = hwnd;
                NativeMethods.TrackMouseEvent(ref tme);
            }
            else if (msg == NativeMethods.WM_MOUSELEAVE)
            {
                NativeMethods.SetFocus(_hWndPrev);
                this.Game.ShouldBeFocused = false;
            }

            return base.WndProc(hwnd, msg, wParam, lParam, ref handled);
        }

        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            _hWnd = this.CreateHostWindow(hwndParent.Handle);

            // Okay, in original DarkLight editor, this.Game wasn't null, but in Evolution it is null. This could be because
            // Evolution uses newer version of AvalonDock..? See also, "PanelSizeChanged"
            if (this.Game != null)
            {
                this.Game.SetHandle(_hWnd);
                _handleSetToGame = true;
            }

            return new HandleRef(this, _hWnd);
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            // Destroy the window and reset our hWnd value
            NativeMethods.DestroyWindow(hwnd.Handle);
            this.Game.ResetHandle();
            _hWnd = IntPtr.Zero;
        }

        /// <summary>
        /// Creates the host window as a child of the parent window.
        /// </summary>
        private IntPtr CreateHostWindow(IntPtr hWndParent)
        {
            // Register our window class
            GamePanelBase<T>.RegisterWindowClass();

            // Create the window
            return NativeMethods.CreateWindowEx(0, GamePanelBase<T>.WindowClassName, "",
               NativeMethods.WS_CHILD | NativeMethods.WS_VISIBLE,
               0, 0, (int)this.Width, (int)this.Height, hWndParent, IntPtr.Zero, IntPtr.Zero, 0);
        }

        /// <summary>
        /// Registers the window class.
        /// </summary>
        private static void RegisterWindowClass()
        {
            NativeMethods.WNDCLASSEX wndClass = new NativeMethods.WNDCLASSEX();
            wndClass.cbSize = (uint)Marshal.SizeOf(wndClass);
            wndClass.hInstance = NativeMethods.GetModuleHandle(null);
            wndClass.lpfnWndProc = NativeMethods.DefaultWindowProc;
            wndClass.lpszClassName = GamePanelBase<T>.WindowClassName;
            wndClass.hCursor = NativeMethods.LoadCursor(IntPtr.Zero, NativeMethods.IDC_ARROW);

            NativeMethods.RegisterClassEx(ref wndClass);
        }
    }
}
