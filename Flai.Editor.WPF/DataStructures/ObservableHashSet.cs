﻿using System;
using System.Collections.ObjectModel;

namespace Flai.Editor.DataStructures
{
    // Really slow compared to HashSet<T>
    // Hmm.. Mayble you could add HashSet<T> to ObservableSet<T> and check containment from it... 
    // Is it possible to keep the base ObservableCollection<T> and the HashSet<T> in sync?
    public class ObservableSet<T> : ObservableCollection<T>
    {
        protected override void InsertItem(int index, T item)
        {
            if (base.Contains(item))
            {
                throw new ArgumentException("Item exists in the set already");
            }

            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, T item)
        {
            int i = base.IndexOf(item);
            if (i >= 0 && i != index)
                throw new ArgumentException("Item exists in the set already");

            base.SetItem(index, item);
        }
    }

    #region ObservableHashSet<T>, not finished, obsolete

    /*
    // Not gonna implement ISet<T> for now because it has so many irrelevant methods
    public class ObservableHashSet<T> : ICollection<T>, IEnumerable<T>, IEnumerable, INotifyCollectionChanged, INotifyPropertyChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly HashSet<T> _hashSet;

        public ObservableHashSet()
            : this(EqualityComparer<T>.Default)
        {
        }

        public ObservableHashSet(IEqualityComparer<T> comparer)
        {
            _hashSet = new HashSet<T>(comparer);
        }

        public ObservableHashSet(IEnumerable<T> collection)
            : this(collection, EqualityComparer<T>.Default)
        {
        }

        public ObservableHashSet(IEnumerable<T> collection, IEqualityComparer<T> comparer)
        {
            _hashSet = new HashSet<T>(collection, comparer);
        }

        #region ICollection<T> Members

        public void Add(T item)
        {
            if (_hashSet.Add(item))
            {
                this.OnPropertyChanged("Count");
                this.OnPropertyChanged("Item[]");
                this.OnCollectionChanged(NotifyCollectionChangedAction.Add, item);
            }
        }

        public void Clear()
        {
            if (_hashSet.Count > 0)
            {
                _hashSet.Clear();
                this.OnPropertyChanged("Count");
                this.OnPropertyChanged("Item[]");
                this.OnCollectionChanged(NotifyCollectionChangedAction.Reset);
            }
        }

        public bool Contains(T item)
        {
            return _hashSet.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _hashSet.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _hashSet.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            if (_hashSet.Remove(item))
            {
                this.OnPropertyChanged("Count");
                this.OnPropertyChanged("Item[]");
                this.OnCollectionChanged(NotifyCollectionChangedAction.Remove, item);
                return true;
            }

            return false;
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return _hashSet.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _hashSet.GetEnumerator();
        }

        #endregion

        private void OnPropertyChanged(string propertyName)
        {
            Common.InvokeIfNotNull(this.PropertyChanged, this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action)
        {
            var handler = this.CollectionChanged;
            if (handler != null)
            {
                handler(this, new NotifyCollectionChangedEventArgs(action));
            }
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item)
        {
            var handler = this.CollectionChanged;
            if (handler != null)
            {
                handler(this, new NotifyCollectionChangedEventArgs(action, item));
            }
        }
    }
    */

    #endregion
}
