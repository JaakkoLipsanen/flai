﻿using System.ComponentModel;
using Flai.DataStructures;
using Flai.Editor.Misc;

namespace Flai.Editor.DataStructures
{
    public interface IObservableUndoRedoStack<T> : IUndoRedoStack<T>, INotifyPropertyChanged
    {
        DelegateCommand UndoCommand { get; }
        DelegateCommand RedoCommand { get; }
    }

    // Meant to be used in WPF
    public class ObservableUndoRedoStack<T> : UndoRedoStack<T>, IObservableUndoRedoStack<T>, INotifyPropertyChanged
        where T : IUndoRedo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public DelegateCommand UndoCommand { get; private set; }
        public DelegateCommand RedoCommand { get; private set; }

        public ObservableUndoRedoStack()
        {
            this.UndoCommand = new DelegateCommand(o => this.Undo(), o => this.CanUndo);
            this.RedoCommand = new DelegateCommand(o => this.Redo(), o => this.CanRedo);
        }

        protected override void OnPush(T value)
        {
            this.PropertyChanged.InvokeIfNotNull(this, "CanUndo");
            this.PropertyChanged.InvokeIfNotNull(this, "CanRedo");
        }

        protected override void OnUndo(T value)
        {
            this.PropertyChanged.InvokeIfNotNull(this, "CanUndo");
            this.PropertyChanged.InvokeIfNotNull(this, "CanRedo");
        }

        protected override void OnRedo(T value)
        {
            this.PropertyChanged.InvokeIfNotNull(this, "CanUndo");
            this.PropertyChanged.InvokeIfNotNull(this, "CanRedo");
        }
    }
}
