﻿using System;
using System.ComponentModel;
using Flai.Editor.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Flai.Editor
{
    public abstract class EditorGameBase : FlaiGame, INotifyPropertyChanged
    {
        public event EventHandler EndTick;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly FpsComponent _fpsComponent;
        private IntPtr _windowHandle;
        private bool _shouldBeFocused;

        public bool ShouldBeFocused
        {
            get { return _shouldBeFocused; }
            set { _shouldBeFocused = value; }
        }

        public Vector2i ControlOffsetFromWindow
        {
            set { base.InputState.MouseOffset = value; }
        }

        public int FPS
        {
            get { return _fpsComponent.FPS; }
        }

        protected EditorGameBase()
        {
            base.InactiveSleepTime = TimeSpan.Zero;
            base.Components.Add((_fpsComponent = new FpsComponent(_serviceContainer)));
        }

        // Not sure if I should handle differently if the handle is already set
        public void SetHandle(IntPtr windowHandle)
        {
            _windowHandle = windowHandle;
            Mouse.WindowHandle = windowHandle;
            this.Initialize();
        }

        // ?
        public void ResetHandle()
        {
            _windowHandle = base.Window.Handle;
            Mouse.WindowHandle = _windowHandle;
        }

        protected override void EndDraw()
        {
            base.EndDraw();
            base.GraphicsDevice.Present();
            this.EndTick.InvokeIfNotNull(this);
        }

        protected override void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = _windowHandle;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged.InvokeIfNotNull(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
