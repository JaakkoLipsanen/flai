﻿using System.ComponentModel;

namespace Flai.Editor
{
    public abstract class GameViewModelBase<T> : INotifyPropertyChanged
        where T : EditorGameBase
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected readonly T _game;
        public T Game
        {
            get { return _game; }
        }

        protected GameViewModelBase(T game)
        {
            _game = game;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
