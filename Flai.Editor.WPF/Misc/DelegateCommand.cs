﻿using System;
using System.Windows.Input;

namespace Flai.Editor.Misc
{
    /// <summary>
    /// Implements ICommand in a delegate friendly way
    /// </summary>
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _executeMethod;
        private readonly Predicate<object> _canExecuteMethod;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public DelegateCommand(Action executeMethod) // ignore parameter
            : this(o => executeMethod(), null)
        {
        }

        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod)  // ignore parameter
            : this(o => executeMethod(), o => canExecuteMethod())
        {
        }

        public DelegateCommand(Action<object> executeMethod)
            : this(executeMethod, null)
        {
        }

        public DelegateCommand(Action<object> executeMethod, Predicate<object> canExecuteMethod)
        {
            if (executeMethod == null)
            {
                throw new ArgumentNullException("executeMethod");
            }

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }

        public bool CanExecute(object parameter)
        {
            return (_canExecuteMethod == null) || _canExecuteMethod(parameter);
        }

        public void Execute(object parameter)
        {
            _executeMethod(parameter);
        }
    }
}
