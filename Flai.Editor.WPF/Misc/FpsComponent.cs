﻿using System;
using Flai.Graphics;

namespace Flai.Editor.Misc
{
    internal class FpsComponent : FlaiDrawableGameComponent
    {
        private static readonly TimeSpan UpdateFrequency = TimeSpan.FromSeconds(0.25f);

        private int _frameRate = 0;
        private int _frameCounter = 0;
        private TimeSpan _elapsedTime = TimeSpan.Zero;

        private int _fps;
        public int FPS
        {
            get { return _fps; }
        }

        internal FpsComponent(FlaiServiceContainer services)
            : base(services)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            _elapsedTime += updateContext.GameTime.ElapsedGameTime;
            if (_elapsedTime > FpsComponent.UpdateFrequency)
            {
                _elapsedTime -= FpsComponent.UpdateFrequency;
                _frameRate = _frameCounter;
                _frameCounter = 0;

                _fps = (int)(_frameRate / FpsComponent.UpdateFrequency.TotalSeconds);
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            _frameCounter++;
        }
    }
}
