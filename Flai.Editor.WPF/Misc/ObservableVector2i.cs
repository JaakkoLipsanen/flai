﻿using System.ComponentModel;

namespace Flai.Editor.Misc
{
    public sealed class ObservableVector2i : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event GenericEvent ValueChanged;

        private Vector2i _innerValue;

        public ObservableVector2i(Vector2i v)
        {
            this.UpdateValue(v);
        }

        [DisplayName("X")]
        [Description("X component of the vector")]
        public int X
        {
            get { return _innerValue.X; }
            set
            {
                if (_innerValue.X != value)
                {
                    _innerValue.X = value;
                    this.PropertyChanged.InvokeIfNotNull(this, "X");
                    this.ValueChanged.InvokeIfNotNull();
                }
            }
        }

        [DisplayName("Y")]
        [Description("Y component of the vector")]
        public int Y
        {
            get { return _innerValue.Y; }
            set
            {
                if (_innerValue.Y != value)
                {
                    _innerValue.Y = value;

                    this.PropertyChanged.InvokeIfNotNull(this, "Y");
                    this.ValueChanged.InvokeIfNotNull();
                }
            }
        }

        public Vector2i Value
        {
            get { return _innerValue; }
            set
            {
                if (_innerValue != value)
                {
                    _innerValue = value;
                    this.PropertyChanged.InvokeIfNotNull(this, "X");
                    this.PropertyChanged.InvokeIfNotNull(this, "Y");
                    this.ValueChanged.InvokeIfNotNull();
                }
            }
        }

        public void UpdateValue(Vector2i newValue)
        {
            if (_innerValue != newValue)
            {
                _innerValue = newValue;
                this.PropertyChanged.InvokeIfNotNull(this, "X");
                this.PropertyChanged.InvokeIfNotNull(this, "Y");
            }
        }

        public override string ToString()
        {
            return _innerValue.ToString();
        }
    }
}

