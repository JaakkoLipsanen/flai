﻿using System.ComponentModel;

namespace Flai.Editor.Misc
{
    public class PropertyChangedEventArgs<T> : PropertyChangedEventArgs
    {
        public T OldValue { get; private set; }
        public T NewValue { get; private set; }

        public PropertyChangedEventArgs(string propertyName, T oldValue, T newValue)
            : base(propertyName)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }
}
