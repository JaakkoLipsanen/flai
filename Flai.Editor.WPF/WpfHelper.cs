﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Flai.Editor
{
    public static class WpfHelper
    {
        public static bool IsMainWindowActive
        {
            get { return Application.Current.MainWindow.IsActive; }
        }

        public static void RemoveToolBarOverflow(ToolBar toolBar)
        {
            FrameworkElement overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }
        }

        // Call in PreviewKeyDown
        public static void DisableKeyboardInput(KeyEventArgs e)
        {
            e.Handled = true;
        }
    }
}