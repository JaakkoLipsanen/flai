﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using Flai.EffectCompiler.Model;
using Microsoft.Win32;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Graphics;

namespace Flai.EffectCompiler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            _targetProfile.Items.Add(GraphicsProfile.Reach);
            _targetProfile.Items.Add(GraphicsProfile.HiDef);

            _targetProfile.SelectedIndex = 1;

            _targetPlatform.Items.Add(TargetPlatform.Windows);
            _targetPlatform.Items.Add(TargetPlatform.WindowsPhone);
            _targetPlatform.Items.Add(TargetPlatform.Xbox360);

            _targetPlatform.SelectedIndex = 0;
        }

        private void _fromFileButton_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = "*.fx|*.txt";

            if (dialog.ShowDialog() == true)
            {
                string text = File.ReadAllText(dialog.FileName);
                _sourceCodeTextBox.Text = text;
                this.LoadByteCodeFromSource(text);
            }
        }

        private void LoadByteCodeFromSource(string text)
        {
            try
            {
                EffectImporter importer = new EffectImporter();
                EffectContent effectContent = new EffectContent
                {
                    Identity = new ContentIdentity { SourceFilename = "myshader.fx" },
                    EffectCode = text,
                };

                EffectProcessor processor = new EffectProcessor();
                processor.DebugMode = EffectProcessorDebugMode.Optimize;
                CompiledEffectContent compiledEffect = processor.Process(effectContent, new FlaiProcessorContext((GraphicsProfile)_targetProfile.SelectedValue, (TargetPlatform)_targetPlatform.SelectedValue));

                byte[] bytes = compiledEffect.GetEffectCode();
                StringBuilder sb = new StringBuilder(bytes.Length * 3);

                sb.AppendLine("#region Effect Byte Code");
                sb.AppendLine();

                sb.Append("byte[] ByteCode = new byte[]").Append(Environment.NewLine).Append("{").Append(Environment.NewLine);
                for (int i = 0; i < bytes.Length;)
                {
                    sb.Append('\t');
                    int max = Math.Min(i + 8, bytes.Length);
                    for (; i < max; i++)
                    {
                        sb.Append(bytes[i]).Append(", ");
                    }

                    sb.Append(Environment.NewLine);
                }

                sb.Append("};").Append(Environment.NewLine);

                sb.AppendLine();
                sb.Append("#endregion");

                _byteCodeTextBox.Text = sb.ToString();
            }
            catch (InvalidContentException e)
            {
                _byteCodeTextBox.Text = e.Message;
            }
        }

        private void _sourceTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            this.LoadByteCodeFromSource(_sourceCodeTextBox.Text);
        }

        private void _copyByteCodeToClipboardButton_Click_1(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(_byteCodeTextBox.Text);
        }

        static MainWindow()
        {
            #region Effect Byte Code

byte[] ByteCode = new byte[]
{
	207, 11, 240, 188, 16, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 1, 
	255, 254, 0, 2, 0, 0, 0, 0, 
	0, 3, 0, 0, 0, 2, 0, 0, 
	96, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 4, 0, 0, 0, 4, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 6, 0, 0, 0, 87, 
	114, 108, 100, 0, 32, 1, 3, 0, 
	0, 2, 0, 0, 0, 200, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 4, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 86, 105, 101, 119, 0, 
	0, 0, 3, 0, 0, 0, 2, 0, 
	0, 48, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 4, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 11, 0, 0, 0, 
	114, 111, 106, 101, 99, 116, 105, 111, 
	0, 0, 3, 0, 0, 0, 1, 0, 
	0, 108, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 4, 0, 0, 0, 
	0, 0, 0, 0, 0, 128, 63, 0, 
	128, 63, 0, 0, 128, 63, 0, 0, 
	63, 13, 0, 0, 0, 65, 109, 98, 
	101, 110, 116, 67, 111, 108, 111, 114, 
	0, 0, 0, 3, 0, 0, 0, 0, 
	0, 0, 160, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 1, 0, 0, 
	1, 0, 0, 0, 205, 204, 204, 61, 
	0, 0, 0, 65, 109, 98, 105, 101, 
	116, 73, 110, 116, 101, 110, 115, 105, 
	121, 0, 0, 0, 0, 1, 0, 0, 
	16, 0, 0, 0, 4, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 2, 0, 0, 0, 15, 0, 
	0, 4, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 80, 97, 115, 115, 49, 
	0, 0, 8, 0, 0, 0, 65, 109, 
	105, 101, 110, 116, 0, 5, 0, 0, 
	1, 0, 0, 0, 3, 0, 0, 0, 
	0, 0, 0, 4, 0, 0, 0, 32, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 108, 0, 0, 0, 136, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 240, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 64, 1, 
	0, 92, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 128, 1, 0, 0, 
	1, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 244, 1, 0, 0, 0, 0, 
	0, 1, 0, 0, 0, 232, 1, 0, 
	0, 0, 0, 0, 2, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 188, 
	0, 0, 184, 1, 0, 0, 147, 0, 
	0, 0, 0, 0, 0, 212, 1, 0, 
	208, 1, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 255, 255, 255, 255, 1, 0, 
	0, 0, 0, 0, 0, 228, 1, 0, 
	0, 2, 255, 255, 254, 255, 22, 0, 
	84, 65, 66, 28, 0, 0, 0, 35, 
	0, 0, 0, 2, 255, 255, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	28, 0, 0, 0, 112, 115, 95, 50, 
	48, 0, 77, 105, 99, 114, 111, 115, 
	102, 116, 32, 40, 82, 41, 32, 72, 
	83, 76, 32, 83, 104, 97, 100, 101, 
	32, 67, 111, 109, 112, 105, 108, 101, 
	32, 57, 46, 50, 54, 46, 57, 53, 
	46, 50, 56, 52, 52, 0, 254, 255, 
	0, 80, 82, 69, 83, 1, 2, 88, 
	254, 255, 56, 0, 67, 84, 65, 66, 
	0, 0, 0, 171, 0, 0, 0, 1, 
	88, 70, 2, 0, 0, 0, 28, 0, 
	0, 0, 1, 0, 32, 168, 0, 0, 
	68, 0, 0, 0, 2, 0, 0, 0, 
	0, 0, 0, 84, 0, 0, 0, 100, 
	0, 0, 116, 0, 0, 0, 2, 0, 
	0, 1, 0, 0, 0, 136, 0, 0, 
	152, 0, 0, 0, 65, 109, 98, 105, 
	110, 116, 67, 111, 108, 111, 114, 0, 
	171, 171, 1, 0, 3, 0, 1, 0, 
	0, 1, 0, 0, 0, 0, 0, 0, 
	0, 0, 128, 63, 0, 0, 128, 63, 
	0, 128, 63, 0, 0, 128, 63, 65, 
	98, 105, 101, 110, 116, 73, 110, 116, 
	110, 115, 105, 116, 121, 0, 171, 171, 
	0, 0, 3, 0, 1, 0, 1, 0, 
	0, 0, 0, 0, 0, 0, 0, 205, 
	204, 61, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 116, 120, 0, 
	105, 99, 114, 111, 115, 111, 102, 116, 
	40, 82, 41, 32, 72, 76, 83, 76, 
	83, 104, 97, 100, 101, 114, 32, 67, 
	109, 112, 105, 108, 101, 114, 32, 57, 
	50, 54, 46, 57, 53, 50, 46, 50, 
	52, 52, 0, 254, 255, 12, 0, 80, 
	83, 73, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 1, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 1, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 254, 255, 2, 0, 67, 76, 73, 
	0, 0, 0, 0, 254, 255, 15, 0, 
	88, 76, 67, 1, 0, 0, 0, 4, 
	80, 160, 2, 0, 0, 0, 0, 0, 
	0, 2, 0, 0, 0, 4, 0, 0, 
	0, 0, 0, 0, 2, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 4, 
	0, 0, 0, 0, 0, 0, 240, 240, 
	240, 15, 15, 15, 15, 255, 255, 0, 
	1, 0, 0, 2, 0, 8, 15, 128, 
	0, 228, 160, 255, 255, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 255, 255, 
	255, 0, 0, 0, 0, 0, 0, 0, 
	212, 1, 0, 0, 0, 2, 254, 255, 
	255, 63, 0, 67, 84, 65, 66, 28, 
	0, 0, 198, 0, 0, 0, 0, 2, 
	255, 3, 0, 0, 0, 28, 0, 0, 
	0, 0, 0, 32, 191, 0, 0, 0, 
	0, 0, 0, 2, 0, 8, 0, 4, 
	0, 0, 100, 0, 0, 0, 116, 0, 
	0, 180, 0, 0, 0, 2, 0, 4, 
	4, 0, 0, 0, 100, 0, 0, 0, 
	0, 0, 0, 185, 0, 0, 0, 2, 
	0, 0, 4, 0, 0, 0, 100, 0, 
	0, 116, 0, 0, 0, 80, 114, 111, 
	101, 99, 116, 105, 111, 110, 0, 171, 
	0, 3, 0, 4, 0, 4, 0, 1, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 86, 
	101, 119, 0, 87, 111, 114, 108, 100, 
	118, 115, 95, 50, 95, 48, 0, 77, 
	99, 114, 111, 115, 111, 102, 116, 32, 
	82, 41, 32, 72, 76, 83, 76, 32, 
	104, 97, 100, 101, 114, 32, 67, 111, 
	112, 105, 108, 101, 114, 32, 57, 46, 
	54, 46, 57, 53, 50, 46, 50, 56, 
	52, 0, 171, 31, 0, 0, 2, 0, 
	0, 128, 0, 0, 15, 144, 9, 0, 
	3, 0, 0, 1, 128, 0, 0, 228, 
	0, 0, 228, 160, 9, 0, 0, 3, 
	0, 2, 128, 0, 0, 228, 144, 1, 
	228, 160, 9, 0, 0, 3, 0, 0, 
	128, 0, 0, 228, 144, 2, 0, 228, 
	9, 0, 0, 3, 0, 0, 8, 128, 
	0, 228, 144, 3, 0, 228, 160, 9, 
	0, 3, 1, 0, 1, 128, 0, 0, 
	128, 4, 0, 228, 160, 9, 0, 0, 
	1, 0, 2, 128, 0, 0, 228, 128, 
	0, 228, 160, 9, 0, 0, 3, 1, 
	4, 128, 0, 0, 228, 128, 6, 0, 
	160, 9, 0, 0, 3, 1, 0, 8, 
	0, 0, 228, 128, 7, 0, 228, 160, 
	0, 0, 3, 0, 0, 1, 192, 1, 
	228, 128, 8, 0, 228, 160, 9, 0, 
	3, 0, 0, 2, 192, 1, 0, 228, 
	9, 0, 228, 160, 9, 0, 0, 3, 
	0, 4, 192, 1, 0, 228, 128, 10, 
	228, 160, 9, 0, 0, 3, 0, 0, 
	192, 1, 0, 228, 128, 11, 0, 228, 
	255, 255, 0, 0, 
};

#endregion
        }
    }
}
