﻿using Microsoft.Xna.Framework.Content.Pipeline;

namespace Flai.EffectCompiler.Model
{
    public class FlaiImporterContext : ContentImporterContext
    {
        private readonly ContentBuildLogger _logger = new FlaiContentBuildLogger();

        public override string IntermediateDirectory { get { return string.Empty; } }
        public override string OutputDirectory { get { return string.Empty; } }

        public override ContentBuildLogger Logger { get { return _logger; } }
        
        public override void AddDependency(string filename) { }
    }
}
