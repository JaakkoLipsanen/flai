﻿using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Graphics;

namespace Flai.EffectCompiler.Model
{
    public class FlaiProcessorContext : ContentProcessorContext
    {
        private readonly ContentBuildLogger _logger = new FlaiContentBuildLogger();
        private readonly OpaqueDataDictionary _parameters = new OpaqueDataDictionary();
        private readonly GraphicsProfile _graphicsProfile;
        private readonly TargetPlatform _targetPlatform;

        public FlaiProcessorContext(GraphicsProfile graphicsProfile, TargetPlatform targetPlatform)
        {
            // TODO: Complete member initialization
            _graphicsProfile = graphicsProfile;
            _targetPlatform = targetPlatform;
        }

        public override TargetPlatform TargetPlatform { get { return _targetPlatform; } }
        public override GraphicsProfile TargetProfile { get { return _graphicsProfile; } }

        public override string BuildConfiguration { get { return string.Empty; } }
        public override string IntermediateDirectory { get { return string.Empty; } }
        public override string OutputDirectory { get { return string.Empty; } }
        public override string OutputFilename { get { return string.Empty; } }

        public override OpaqueDataDictionary Parameters { get { return _parameters; } }
        public override ContentBuildLogger Logger { get { return _logger; } }


        public override void AddDependency(string filename) { }
        public override void AddOutputFile(string filename) { }

        public override TOutput Convert<TInput, TOutput>(TInput input, string processorName, OpaqueDataDictionary processorParameters) { throw new NotImplementedException(); }
        public override TOutput BuildAndLoadAsset<TInput, TOutput>(ExternalReference<TInput> sourceAsset, string processorName, OpaqueDataDictionary processorParameters, string importerName) { throw new NotImplementedException(); }
        public override ExternalReference<TOutput> BuildAsset<TInput, TOutput>(ExternalReference<TInput> sourceAsset, string processorName, OpaqueDataDictionary processorParameters, string importerName, string s) { throw new NotImplementedException(); }
    }
}
