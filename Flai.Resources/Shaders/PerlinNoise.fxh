
texture PermutationTexure;
texture PermutationGradientTexture;

int Octaves = 16;
float2 Frequency = float2(1, 1); // Frequency of the noise. ViewportSize * SomeValue
float Amplitude = 1;
float Persistance = 0.65f;

sampler PermutationSampler = sampler_state 
{
    texture = <PermutationTexure>;
    AddressU  = Wrap;        
    AddressV  = Wrap;
    MAGFILTER = POINT;
    MINFILTER = POINT;
    MIPFILTER = NONE;   
};

sampler PermutationGradientSampler = sampler_state 
{
    texture = <PermutationGradientTexture>;
    AddressU  = Wrap;        
    AddressV  = Clamp;
    MAGFILTER = POINT;
    MINFILTER = POINT;
    MIPFILTER = NONE;
};

float3 fade(float3 t)
{
	return t * t * t * (t * (t * 6 - 15) + 10); // new curve
}
float4 perm2d(float2 p)
{
	return tex2D(PermutationSampler, p);
}
float gradperm(float x, float3 p)
{
	return dot(tex1D(PermutationGradientSampler, x), p);
}

float SampleNoise3DRaw(float3 p)
{
	float3 P = fmod(floor(p), 256.0);	// FIND UNIT CUBE THAT CONTAINS POINT
  	p -= floor(p);                      // FIND RELATIVE X,Y,Z OF POINT IN CUBE.
	float3 f = fade(p);                 // COMPUTE FADE CURVES FOR EACH OF X,Y,Z.

	P = P / 256.0;
	const float one = 1.0 / 256.0;
	
    // HASH COORDINATES OF THE 8 CUBE CORNERS
	float4 AA = perm2d(P.xy) + P.z;
 
	// AND ADD BLENDED RESULTS FROM 8 CORNERS OF CUBE
  	return lerp( lerp( lerp( gradperm(AA.x, p ),  
                             gradperm(AA.z, p + float3(-1, 0, 0) ), f.x),
                       lerp( gradperm(AA.y, p + float3(0, -1, 0) ),
                             gradperm(AA.w, p + float3(-1, -1, 0) ), f.x), f.y),
                             
                 lerp( lerp( gradperm(AA.x+one, p + float3(0, 0, -1) ),
                             gradperm(AA.z+one, p + float3(-1, 0, -1) ), f.x),
                       lerp( gradperm(AA.y+one, p + float3(0, -1, -1) ),
                             gradperm(AA.w+one, p + float3(-1, -1, -1) ), f.x), f.y), f.z);
}

float SampleNoise3D(float3 input)
{
	float total = 0.0f;
	float frequency = Frequency;
	float amplitude = Amplitude;
	for(int i = 0; i < Octaves; i++)
	{
		total += SampleNoise3DRaw(input * frequency) * amplitude;
		frequency *= 2;
		amplitude *= Persistance;
	}

	return total;
}

float SampleNoise2DWithTime(float3 input)
{
	float total = 0.0f;
	float frequency = Frequency;
	float amplitude = Amplitude;
	for(int i = 0; i < Octaves; i++)
	{
		total += SampleNoise3DRaw(float3(float2(input.x, input.y) * frequency, input.z)) * amplitude;
		frequency *= 2;
		amplitude *= Persistance;
	}

	return total;
}