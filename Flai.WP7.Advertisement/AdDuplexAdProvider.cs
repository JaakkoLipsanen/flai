﻿using AdDuplex;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using System;
using Microsoft.Xna.Framework.Graphics;
using AdDuplexManager = AdDuplex.Xna.AdManager;

namespace Flai.Advertisiments
{
    public class AdDuplexAdProvider : AdProvider
    {
        private AdDuplexManager _adManager;

        public event EventHandler AdError;
        public event EventHandler AdAvailable;

        public override bool Initialized
        {
            get { return _adManager != null; }
        }

        public AdDuplexAdProvider(FlaiServiceContainer services)
            : base(services)
        {
        }

        public void Initialize(Game game, string appId)
        {
            _adManager = new AdDuplexManager(game, appId)
            {
                Enabled = _enabled,
                Visible = _visible,
                RefreshInterval = 30,
                IsTest = false,
            };

            _adManager.AdLoaded += this.OnAdAvailable;
            _adManager.AdLoadingError += this.OnErrorOccured;
        }

        public override void LoadContent()
        {
            if (!this.Initialized)
            {
                throw new InvalidOperationException("Ad provider must be initialized before calling LoadContent");
            }

            _adManager.LoadContent();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            if (_adManager != null)
            {
                _adManager.Update(((IXnaGameTimeProvider)updateContext).GameTime);
            }
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_adManager != null)
            {
                Vector2i topLeftPosition = _position - AdManager.AdSize / 2;
                _adManager.Draw(graphicsContext.SpriteBatch.InnerSpriteBatch, topLeftPosition);
            }
        }

        protected override void OnEnabledChanged()
        {
            if (_adManager != null)
            {
                _adManager.Enabled = _enabled;
            }
        }

        protected override void OnVisibleChanged()
        {
            if (_adManager != null)
            {
                _adManager.Visible = _visible;
            }
        }

        private void OnErrorOccured(object sender, AdLoadingErrorEventArgs e)
        {
            var handler = this.AdError;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        private void OnAdAvailable(object sender, AdLoadedEventArgs e)
        {
            var handler = this.AdAvailable;
            if (handler != null)
            {
                handler(sender, e);
            }
        }
    }
}
