using System;
using System.Collections.Generic;
using Flai.Graphics;
using Microsoft.Advertising;

namespace Flai.Advertisiments
{
    public enum AdProviderType
    {
        MicrosoftAds = 1,
        AdDuplexAds = 2,
        HouseAds = 3,
    }

    public class AdManager : FlaiDrawableGameComponent, IAdManager
    {
        public static readonly Vector2i AdSize = new Vector2i(480, 80);

        private AdProviderType _activeAdProvider = AdProviderType.HouseAds;
        private Vector2i _adPosition = new Vector2i(400, 240 - AdSize.Y - 8);

        protected readonly MicrosoftAdProvider _microsoftAds;
        protected readonly AdDuplexAdProvider _adDuplexAds;
        protected readonly HouseAdProvider _houseAds;

        protected readonly Dictionary<AdProviderType, AdProvider> _adProviders =
            new Dictionary<AdProviderType, AdProvider>();

        public Vector2i AdPosition
        {
            get { return _adPosition; }
            set
            {
                if (_adPosition != value)
                {
                    _adPosition = value;
                    foreach (AdProvider adProvider in _adProviders.Values)
                    {
                        adProvider.Position = value;
                    }
                }
            }
        }

        public AdManager(FlaiServiceContainer services)
            : base(services)
        {
            // By default, ad manager is not enabled nor visible
            _enabled = false;
            _visible = false;

            // Since AdManager.Enabled is false by default, all the ad providers all false by default also
            _microsoftAds = new MicrosoftAdProvider(services)
            {
                Enabled = false,
                Visible = false,
                Position = _adPosition,
            };
            _adDuplexAds = new AdDuplexAdProvider(services)
            {
                Enabled = false,
                Visible = false,
                Position = _adPosition,
            };
            _houseAds = new HouseAdProvider(services)
            {
                Enabled = false,
                Visible = false,
                Position = _adPosition,
            };

            _microsoftAds.AdError += this.OnMicrosoftAdError;
            _microsoftAds.AdAvailable += this.OnMicrosoftAdAvailable;

            _adDuplexAds.AdError += this.OnAdDuplexAdError;
            _adDuplexAds.AdAvailable += this.OnAdDuplexAdAvailable;

            _adProviders.Add(AdProviderType.MicrosoftAds, _microsoftAds);
            _adProviders.Add(AdProviderType.AdDuplexAds, _adDuplexAds);
            _adProviders.Add(AdProviderType.HouseAds, _houseAds);

            services.Add<IAdManager>(this);
        }

        public void InitializeMicrosoftAds(string applicationId, string adUnitId)
        {
            _microsoftAds.Initialize(FlaiGame.Current, applicationId);
            _microsoftAds.InitializeAd(adUnitId);
        }

        public void InitializeAdDuplexAds(string appId)
        {
            _adDuplexAds.Initialize(FlaiGame.Current, appId);
        }

        public void InitializeHouseAds(params HouseAd[] houseAds)
        {
            _houseAds.Initialize(houseAds);
        }

        protected override void LoadContent()
        {
            foreach (AdProvider provider in _adProviders.Values)
            {
                if (provider.Initialized)
                {
                    provider.LoadContent();
                }
            }
        }

        public override void Update(UpdateContext updateContext)
        {
            if (_enabled)
            {
                foreach (AdProvider provider in _adProviders.Values)
                {
                    provider.Update(updateContext);
                }
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            if (_visible)
            {
                _adProviders[_activeAdProvider].Draw(graphicsContext);
            }
        }

        private void OnMicrosoftAdError(object sender, AdErrorEventArgs e)
        {
            // If the error is ServerSideError ( I am getting this on phone with Finnish locale, so this is probably the error that is being got if ad is not available for users country ), disable Microsoft ads
            // Do this here because switch won't get called if MS ads arent active ad provider
            if (e.ErrorCode == ErrorCode.ServerSideError)
            {
                _microsoftAds.Enabled = false;
            }

            if (_activeAdProvider == AdProviderType.MicrosoftAds)
            {
                switch (e.ErrorCode)
                {
                    // If the error is caused by client network failure, switch to house ads
                    case ErrorCode.NetworkConnectionFailure:
                        this.SetActiveAdProvider(AdProviderType.HouseAds);
                        break;

                    // Else just change to AdDuplex
                    default:
                        if (_adDuplexAds.Initialized)
                        {
                            // If MS ads fails, switch to AdDuplex
                            _adDuplexAds.Enabled = true;
                            this.SetActiveAdProvider(AdProviderType.AdDuplexAds);
                        }
                        else
                        {
                            this.SetActiveAdProvider(AdProviderType.HouseAds);
                        }
                        
                        break;
                }
            }
        }

        private void OnMicrosoftAdAvailable(object sender, EventArgs e)
        {
            // If ads are available, should MS ads always be enabled? Though this won't probably get called if Microsoft ads are disabled
            _microsoftAds.Enabled = true;

            this.SetActiveAdProvider(AdProviderType.MicrosoftAds);

            // If MS ads are available, turn AdDuplex off
            _adDuplexAds.Enabled = false;
        }

        private void OnAdDuplexAdError(object sender, EventArgs e)
        {
            // If AdDuplex is the active ad provider and there's an error, use house ads
            if (_activeAdProvider == AdProviderType.AdDuplexAds)
            {
                this.SetActiveAdProvider(AdProviderType.HouseAds);
            }
        }

        private void OnAdDuplexAdAvailable(object sender, EventArgs e)
        {
            // If AdDuplex ads are available and active ad provider is house ads, use AdDuplex ads
            if (_activeAdProvider == AdProviderType.HouseAds)
            {
                this.SetActiveAdProvider(AdProviderType.AdDuplexAds);
            }
        }

        private void SetActiveAdProvider(AdProviderType activeProviderType)
        {
            if (_activeAdProvider == activeProviderType)
            {
                return;

            }
            if (!_adProviders[activeProviderType].Initialized)
            {
                throw new ArgumentException("Ad provider must be initialized before setting it active");
            }

            _activeAdProvider = activeProviderType;
            foreach (KeyValuePair<AdProviderType, AdProvider> kvp in _adProviders)
            {
                kvp.Value.Visible = _visible && (kvp.Key == activeProviderType);
            }
        }

        protected override void OnEnabledChanged()
        {
            foreach (AdProvider provider in _adProviders.Values)
            {
                provider.Enabled = _enabled;
            }
        }

        protected override void OnVisibleChanged()
        {
            foreach (AdProvider provider in _adProviders.Values)
            {
                provider.Visible = _visible && provider == _adProviders[_activeAdProvider];
            }
        }
    }
}
