using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace Flai.Advertisiments
{
    public abstract class AdProvider
    {
        protected readonly FlaiServiceContainer _services;

        protected bool _enabled = false;
        protected bool _visible = false;

        protected Vector2i _position;

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    this.OnEnabledChanged();
                }
            }
        }

        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (_visible != value)
                {
                    _visible = value;
                    this.OnVisibleChanged();
                }
            }
        }

        public Vector2i Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    this.OnPositionChanged();
                }
            }
        }

        public Rectangle DisplayArea
        {
            get
            {
                return new Rectangle(
                    _position.X - AdManager.AdSize.X / 2,
                    _position.Y - AdManager.AdSize.Y / 2,
                    AdManager.AdSize.X,
                    AdManager.AdSize.Y);
            }
        }

        public abstract bool Initialized { get; }

        public AdProvider(FlaiServiceContainer services)
        {
            _services = services;
        }

        public virtual void LoadContent() { }

        public void Update(UpdateContext updateContext)
        {
            if (_enabled && this.Initialized)
            {
                this.UpdateInner(updateContext);
            }
        }
        protected virtual void UpdateInner(UpdateContext updateContext) { }

        public void Draw(GraphicsContext graphicsContext)
        {
            if (_visible && this.Initialized )
            {
                this.DrawInner(graphicsContext);
            }
        }
        protected virtual void DrawInner(GraphicsContext graphicsContext) { }

        protected virtual void OnEnabledChanged() { }
        protected virtual void OnVisibleChanged() { }

        protected virtual void OnPositionChanged() { }
    }
}
