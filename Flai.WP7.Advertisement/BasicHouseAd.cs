using System;
using Flai.Graphics;

namespace Flai.Advertisiments
{
    public class BasicHouseAd : HouseAd
    {
        private Sprite _sprite;

        public BasicHouseAd(string productId)
            : base(productId)
        {
        }

        public void LoadContent(Sprite sprite)
        {
            if (sprite == null)
            {
                throw new ArgumentNullException("sprite");
            }

            _sprite = sprite; 
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _sprite.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_sprite != null)
            {
                graphicsContext.SpriteBatch.Begin();
                graphicsContext.SpriteBatch.Draw(_sprite, base.DisplayArea);
                graphicsContext.SpriteBatch.End();
            }
        }
    }
}
