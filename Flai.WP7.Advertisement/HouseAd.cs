
using Flai.Graphics;
using Flai.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Flai.Advertisiments
{
    public abstract class HouseAd
    {
        private readonly string _marketplaceProductId;
        private Rectangle _displayRectangle;

        private bool _enabled = false;
        private bool _visible = false;

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    this.OnEnabledChanged();
                }
            }
        }

        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (_visible != value)
                {
                    _visible = value;
                    this.OnVisibleChanged();
                }
            }
        }

        public Rectangle DisplayArea
        {
            get { return _displayRectangle; }
            set { _displayRectangle = value; }
        }

        protected HouseAd(string marketplaceProductId)
        {
            _marketplaceProductId = (marketplaceProductId ?? "").Trim();
        }

        public void Update(UpdateContext updateContext)
        {
            if (_enabled && _visible)
            {
                if (!string.IsNullOrEmpty(_marketplaceProductId))
                {
                    foreach (TouchLocation touchLocation in updateContext.InputState.GetTouchLocationsAt(_displayRectangle))
                    {
                        if (touchLocation.State == TouchLocationState.Released)
                        {
                            this.ShowMarketplace();
                        }
                    }
                }
            }

            this.UpdateInner(updateContext);
        }
        protected virtual void UpdateInner(UpdateContext updateContext) { }

        public void Draw(GraphicsContext graphicsContext)
        {
            if (_visible)
            {
                this.DrawInner(graphicsContext);
            }
        }
        protected virtual void DrawInner(GraphicsContext graphicsContext) { }

        private void ShowMarketplace()
        {
            if (!string.IsNullOrEmpty(_marketplaceProductId))
            {
                ApplicationInfo.OpenMarketplaceApplicationPage(_marketplaceProductId);
            }
        }

        protected virtual void OnEnabledChanged() { }
        protected virtual void OnVisibleChanged() { }
    }
}
