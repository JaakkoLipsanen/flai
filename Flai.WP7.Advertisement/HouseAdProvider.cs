﻿
using Flai.Graphics;
using System;
using Flai.Misc;

namespace Flai.Advertisiments
{
    public class HouseAdProvider : AdProvider
    {
        private readonly TimeSpan MinimumTimeBetweenUpdates = TimeSpan.FromSeconds(5);
        private readonly TimeSpan MaximumTimeBetweenUpdates = TimeSpan.FromSeconds(120);

        private TimeSpan _timeBetweenUpdates = TimeSpan.FromSeconds(20);
        private TimeSpan _timeSinceLastUpdate = TimeSpan.Zero;
        private HouseAd[] _houseAds;
        private int _activeHouseAdIndex = -1;

        private bool _initialized = false;

        public override bool Initialized
        {
            get { return _initialized; }
        }

        public TimeSpan TimeBetweenUpdates
        {
            get { return _timeBetweenUpdates; }
            set
            {
                _timeBetweenUpdates = value.ClampToRange(MinimumTimeBetweenUpdates, MaximumTimeBetweenUpdates);
            }
        }

        public HouseAdProvider(FlaiServiceContainer services)
            : base(services)
        {
        }

        public void Initialize(params HouseAd[] houseAds)
        {
            _houseAds = (houseAds ?? new HouseAd[0]);

            // If _houseAds contains ads, then set the ad in index 0 to be the active ad
            _activeHouseAdIndex = (_houseAds.Length > 0) ? Global.Random.Next(_houseAds.Length) : -1;

            for(int i = 0; i < houseAds.Length; i++)
            {
                _houseAds[i].DisplayArea = base.DisplayArea;
                _houseAds[i].Enabled = _enabled && i == _activeHouseAdIndex;
                _houseAds[i].Visible = _visible && i == _activeHouseAdIndex;
            }

            _initialized = true;
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _timeSinceLastUpdate += updateContext.GameTime.XnaGameTime.ElapsedGameTime;
            if (_timeSinceLastUpdate > _timeBetweenUpdates && _houseAds.Length != 0)
            {
                _timeSinceLastUpdate = TimeSpan.Zero;

                _activeHouseAdIndex++;
                if (_activeHouseAdIndex >= _houseAds.Length)
                {
                    _activeHouseAdIndex = 0;
                }

                this.UpdateActiveAd();
            }

            if (_activeHouseAdIndex != -1)
            {
                _houseAds[_activeHouseAdIndex].Update(updateContext);
            }
        }

        private void UpdateActiveAd()
        {
            for (int i = 0; i < _houseAds.Length; i++)
            {
                _houseAds[i].Enabled = _enabled && _activeHouseAdIndex == i;
                _houseAds[i].Visible = _visible && _activeHouseAdIndex == i;
            }
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_activeHouseAdIndex != -1)
            {
                _houseAds[_activeHouseAdIndex].Draw(graphicsContext);
            }
        }

        protected override void OnEnabledChanged()
        {
            if (_initialized && _houseAds.Length != 0)
            {
                for (int i = 0; i < _houseAds.Length; i++)
                {
                    _houseAds[i].Enabled = _enabled && _activeHouseAdIndex == i;
                }
            }
        }

        protected override void OnVisibleChanged()
        {
            if (_initialized && _houseAds.Length != 0)
            {
                for (int i = 0; i < _houseAds.Length; i++)
                {
                    _houseAds[i].Visible = _visible && _activeHouseAdIndex == i;
                }
            }
        }

        protected override void OnPositionChanged()
        {
            if (_initialized && _houseAds.Length != 0)
            {
                for (int i = 0; i < _houseAds.Length; i++)
                {
                    _houseAds[i].DisplayArea = base.DisplayArea;
                }
            }
        }
    }
}
