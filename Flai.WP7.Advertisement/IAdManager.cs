﻿
namespace Flai.Advertisiments
{
    public interface IAdManager
    {
        Vector2i AdPosition { get; set; }
        bool Enabled { get; set; }
        bool Visible { get; set; }
    }
}
