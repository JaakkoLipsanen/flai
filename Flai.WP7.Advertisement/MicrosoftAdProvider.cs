
using Flai.Graphics;
using Microsoft.Advertising;
using Microsoft.Advertising.Mobile.Xna;
using Microsoft.Xna.Framework;
using System;

namespace Flai.Advertisiments
{
    public class MicrosoftAdProvider : AdProvider
    {
        private AdGameComponent _adComponent;
        private DrawableAd _currentAd;

        public override bool Initialized
        {
            get { return AdGameComponent.Initialized; }
        }

        public event EventHandler<AdErrorEventArgs> AdError;
        public event EventHandler AdAvailable;

        private const float RefreshRate = 60f; // 60 seconds
        private float _timeSinceLastRefresh = 0f;

        public MicrosoftAdProvider(FlaiServiceContainer services)
            : base(services)
        {
        }

        public void Initialize(Game game, string applicationId)
        {
            if (!AdGameComponent.Initialized)
            {
                AdGameComponent.Initialize(game, applicationId);
                _adComponent = AdGameComponent.Current;
                _adComponent.Visible = false;
                _adComponent.Enabled = true;
            }
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            if (_adComponent != null)
            {
                _adComponent.Update(((IXnaGameTimeProvider)updateContext).GameTime);

                if (_enabled && _visible && _currentAd != null && !_currentAd.AutoRefreshEnabled)
                {
                    _timeSinceLastRefresh += updateContext.DeltaSeconds;
                    if(_timeSinceLastRefresh > RefreshRate)
                    {
                        _timeSinceLastRefresh -= RefreshRate;
                        _currentAd.Refresh();
                    }
                }
            }
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_adComponent != null)
            {
                _adComponent.Draw(graphicsContext.GameTime);
            }
        }

        public DrawableAd InitializeAd(string adUnitId)
        {
            if (!this.Initialized)
            {
                throw new InvalidOperationException("Ad provider must be initialized before creating the ad");
            }

            if (_currentAd != null)
            {
                _currentAd.ErrorOccurred -= this.OnErrorOccured;
                _currentAd.AdRefreshed -= this.OnAdAvailable;

                _adComponent.RemoveAd(_currentAd);
            }
            
            _currentAd = _adComponent.CreateAd(adUnitId, base.DisplayArea, true);
            _currentAd.ErrorOccurred += this.OnErrorOccured;
            _currentAd.AdRefreshed += this.OnAdAvailable;

            // Disable border and shadow to reduce drawing. Fill rate is a big bottle neck on 1st gen WP devices
            _currentAd.BorderEnabled = false;
            _currentAd.DropShadowEnabled = false;

            return _currentAd;
        }

        protected override void OnEnabledChanged()
        {
            if (_adComponent != null)
            {
                _adComponent.Enabled = base.Enabled;
            }
        }

        protected override void OnVisibleChanged()
        {
            if (_adComponent != null)
            {
                _adComponent.Visible = base.Visible;
            }
        }

        protected override void OnPositionChanged()
        {
            if (_currentAd != null)
            {
                _currentAd.DisplayRectangle = new Rectangle(_position.X - AdManager.AdSize.X / 2, _position.Y - AdManager.AdSize.Y / 2, AdManager.AdSize.X, AdManager.AdSize.Y);
            }
        }

        private void OnErrorOccured(object sender, AdErrorEventArgs e)
        {
            var handler = this.AdError;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        private void OnAdAvailable(object sender, EventArgs e)
        {
            var handler = this.AdAvailable;
            if (handler != null)
            {
                handler(sender, e);
            }
        }
    }
}
