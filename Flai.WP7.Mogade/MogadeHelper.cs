using Mogade.WindowsPhone;

namespace Flai.Mogade
{
    internal static class MogadeHelper
    {
        internal static IMogadeClient CreateInstance(string gameKey, string secret)
        {
            MogadeConfiguration.Configuration(c => c.UsingUniqueIdStrategy(UniqueIdStrategy.UserId));
            return MogadeClient.Initialize(gameKey, secret);
        }
    }
}
