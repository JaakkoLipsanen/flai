using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Mogade;

namespace Flai.Mogade
{
    public abstract class MogadeLeaderboard
    {
        private readonly ReadOnlyCollection<Score> _readOnlyScores;

        protected readonly IMogadeManager _mogadeManager;
        protected readonly List<Score> _scores = new List<Score>();
        protected readonly string _leaderboardId;

        protected LeaderboardScope _scope = LeaderboardScope.Daily;
        protected bool _isLoading = false;
        protected bool _canLoadMoreScores = true;

        protected LeaderboardRequest _latestRequest;

        public string LeaderboardId
        {
            get { return _leaderboardId; }
        }

        public LeaderboardScope Scope
        {
            get { return _scope; }
            set
            {
                if (_scope != value)
                {
                    _scope = value;
                    this.OnPropertyChanged("Scope");
                }
            }
        }

        public bool IsLoading
        {
            get { return _isLoading; }
        }

        public bool CanLoadMoreScores
        {
            get { return _canLoadMoreScores; }
        }

        public ReadOnlyCollection<Score> Scores
        {
            get { return _readOnlyScores; }
        }

        public MogadeLeaderboard(IMogadeManager mogadeManager, string leaderboardId)
        {
            if (mogadeManager == null)
            {
                throw new ArgumentNullException("mogadeManager");
            }

            if (string.IsNullOrEmpty(leaderboardId))
            {
                throw new ArgumentException("leaderboardId");
            }

            _leaderboardId = leaderboardId;
            _mogadeManager = mogadeManager;

            _readOnlyScores = new ReadOnlyCollection<Score>(_scores);
        }

        public void GetRank(int userScore, LeaderboardScope scope, Action<Response<int>, LeaderboardScope> userCallback)
        {
            if (userCallback == null)
            {
                return;
            }

            LeaderboardScope tempScope = scope;
            Action<Response<int>> callback = (response) =>
            {
                userCallback(response, tempScope);
            };
            _mogadeManager.GetRank(_leaderboardId, userScore, scope, callback);
        }

        public void GetRank(string username, LeaderboardScope scope, Action<Response<int>, LeaderboardScope> userCallback)
        {
            if (userCallback == null)
            {
                return;
            }

            LeaderboardScope tempScope = scope;
            Action<Response<int>> callback = (response) =>
            {
                userCallback(response, tempScope);
            };
            _mogadeManager.GetRank(_leaderboardId, username, scope, callback);
        }

        public void GetRanks(int userScore, LeaderboardScope[] scopes, Action<Response<Ranks>> userCallback)
        {
            if (userCallback == null)
            {
                return;
            }

            _mogadeManager.GetRanks(_leaderboardId, userScore, scopes, userCallback);
        }

        public void GetRanks(string username, LeaderboardScope[] scopes, Action<Response<Ranks>> userCallback)
        {
            if (userCallback == null)
            {
                return;
            }

            _mogadeManager.GetRanks(_leaderboardId, username, scopes, userCallback);
        }

        protected virtual void OnPropertyChanged(string changedPropertyName) { }

        protected struct LeaderboardRequest : IEquatable<LeaderboardRequest>
        {
            private const int DefaultRecordCount = 10;

            public Guid Guid { get; private set; }
            public LeaderboardScope Scope { get; private set; }

            public int? Page { get; private set; }
            public int Records { get; private set; }

            public string UserName { get; private set; }

            public LeaderboardRequest(LeaderboardScope scope, int page)
                : this(scope, page, LeaderboardRequest.DefaultRecordCount)
            {
            }

            public LeaderboardRequest(LeaderboardScope scope, int page, int records)
                : this()
            {
                this.Scope = scope;
                this.Page = page;
                this.Records = records;

                this.Guid = Guid.NewGuid();
            }

            public LeaderboardRequest(LeaderboardScope scope, string userName)
                : this(scope, userName, LeaderboardRequest.DefaultRecordCount)
            {

            }

            public LeaderboardRequest(LeaderboardScope scope, string userName, int records)
                : this()
            {
                this.Scope = scope;

                this.UserName = userName;
                this.Records = records;

                this.Guid = Guid.NewGuid();
            }

            public override bool Equals(object obj)
            {
                if (obj is LeaderboardRequest)
                {
                    return this.Equals((LeaderboardRequest)obj);
                }
                return base.Equals(obj);
            }

            #region IEquatable<LeaderboardRequest> Members

            public bool Equals(LeaderboardRequest other)
            {
                return this.Guid == other.Guid;
            }

            #endregion

            public override int GetHashCode()
            {
                return this.Guid.GetHashCode();
            }

            public override string ToString()
            {
                return string.Format(
                    "LeaderboardRequest: { Guid = {0}, Scope = {1}, Page = {2}, Records = {3}, UserName = {4}",
                    this.Guid, this.Scope, this.Page, this.Records, this.UserName);
            }
        }
    }
}
