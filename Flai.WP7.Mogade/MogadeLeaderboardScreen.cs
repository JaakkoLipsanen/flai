using Flai.ScreenManagement;

namespace Flai.Mogade
{
    public abstract class LeaderboardScreen : GameScreen
    {
        protected readonly string _leaderboardID;
        protected IMogadeManager _mogadeManager;

        protected LeaderboardScreen(string leaderboardId)
        {
            _leaderboardID = leaderboardId;
        }

        protected sealed override void LoadContent(bool instancePreserved)
        {
            if (instancePreserved)
            {
                return;
            }

            _mogadeManager = base.Services.GetService<IMogadeManager>();
        }

        protected virtual void InnerActivate(bool instancePreserved) { }
    }
}
