
using System;
using System.Collections.Generic;
using Mogade;
using Mogade.WindowsPhone;

namespace Flai.Mogade
{
    public class MogadeManager : FlaiService, IMogadeManager
    {
        private readonly IMogadeClient _mogadeClient;

        // Can't use FlaiServiceContaienr because I don't want Flai.Mogade to depend on Flai
        public MogadeManager(FlaiServiceContainer services, string gameKey, string secret)
            : base(services)
        {
            _mogadeClient = MogadeHelper.CreateInstance(gameKey, secret);

            // Log application start. Not sure if this can throw an exception, but lets make sure that the game won't crash if it does
            try
            {
                _mogadeClient.LogApplicationStart();
            }
            catch { }

            _services.Add<IMogadeManager>(this);
        }

        #region IMogadeClient Members

        public void AchievementEarned(string achievementId, string userName, Action<Response<Achievement>> callback)
        {
            _mogadeClient.AchievementEarned(achievementId, userName, callback);
        }

        public string ApiVersion
        {
            get { return _mogadeClient.ApiVersion; }
        }

        public IDriver Driver
        {
            get { return _mogadeClient.Driver; }
        }

        public void GetAchievements(Action<Response<ICollection<Achievement>>> callback)
        {
            _mogadeClient.GetAchievements(callback);
        }

        public void GetAssets(Action<Response<IList<Asset>>> callback)
        {
            _mogadeClient.GetAssets(callback);
        }

        public void GetEarnedAchievements(string userName, Action<Response<ICollection<string>>> callback)
        {
            _mogadeClient.GetEarnedAchievements(userName, callback);
        }

        public void GetLeaderboard(string leaderboardId, LeaderboardScope scope, string userName, int records, Action<Response<LeaderboardScores>> callback)
        {
            _mogadeClient.GetLeaderboard(leaderboardId, scope, userName, records, callback);
        }

        public void GetLeaderboard(string leaderboardId, LeaderboardScope scope, int page, int records, Action<Response<LeaderboardScores>> callback)
        {
            _mogadeClient.GetLeaderboard(leaderboardId, scope, page, records, callback);
        }

        public void GetLeaderboard(string leaderboardId, LeaderboardScope scope, int page, Action<Response<LeaderboardScores>> callback)
        {
            _mogadeClient.GetLeaderboard(leaderboardId, scope, page, callback);
        }

        public void GetLeaderboardCount(string leaderboardId, LeaderboardScope scope, Action<Response<int>> callback)
        {
            _mogadeClient.GetLeaderboardCount(leaderboardId, scope, callback);
        }

        public void GetLeaderboardWithPlayerStats(string leaderboardId, LeaderboardScope scope, string userName, int page, int records, Action<Response<LeaderboardScoresWithPlayerStats>> callback)
        {
            _mogadeClient.GetLeaderboardWithPlayerStats(leaderboardId, scope, userName, page, records, callback);
        }

        public void GetPlayerScore(string leaderboardId, LeaderboardScope scope, string userName, Action<Response<Score>> callback)
        {
            _mogadeClient.GetPlayerScore(leaderboardId, scope, userName, callback);
        }

        public void GetRank(string leaderboardId, int score, LeaderboardScope scope, Action<Response<int>> callback)
        {
            _mogadeClient.GetRank(leaderboardId, score, scope, callback);
        }

        public void GetRank(string leaderboardId, string userName, LeaderboardScope scope, Action<Response<int>> callback)
        {
            _mogadeClient.GetRank(leaderboardId, userName, scope, callback);
        }

        public void GetRanks(string leaderboardId, int score, LeaderboardScope[] scopes, Action<Response<Ranks>> callback)
        {
            _mogadeClient.GetRanks(leaderboardId, score, scopes, callback);
        }

        public void GetRanks(string leaderboardId, int score, Action<Response<Ranks>> callback)
        {
            _mogadeClient.GetRanks(leaderboardId, score, callback);
        }

        public void GetRanks(string leaderboardId, string userName, LeaderboardScope[] scopes, Action<Response<Ranks>> callback)
        {
            _mogadeClient.GetRanks(leaderboardId, userName, scopes, callback);
        }

        public void GetRanks(string leaderboardId, string userName, Action<Response<Ranks>> callback)
        {
            _mogadeClient.GetRanks(leaderboardId, userName, callback);
        }

        public void GetRivals(string leaderboardId, LeaderboardScope scope, string userName, Action<Response<IList<Score>>> callback)
        {
            _mogadeClient.GetRivals(leaderboardId, scope, userName, callback);
        }

        public string GetUniqueIdentifier()
        {
            return _mogadeClient.GetUniqueIdentifier();
        }

        public ICollection<string> GetUserNames()
        {
            return _mogadeClient.GetUserNames();
        }

        public void LogApplicationStart()
        {
            _mogadeClient.LogApplicationStart();
        }

        public void LogCustomStat(int index)
        {
            _mogadeClient.LogCustomStat(index);
        }

        public void LogError(string subject, string details)
        {
            _mogadeClient.LogError(subject, details);
        }

        public void RemoveUserName(string userName)
        {
            _mogadeClient.RemoveUserName(userName);
        }

        public void Rename(string currentUserName, string newUserName, Action<Response<bool>> callback)
        {
            _mogadeClient.Rename(currentUserName, newUserName, callback);
        }

        public void SaveScore(string leaderboardId, Score score, Action<Response<SavedScore>> callback)
        {
            _mogadeClient.SaveScore(leaderboardId, score, callback);
        }

        public void SaveUserName(string userName)
        {
            _mogadeClient.SaveUserName(userName);
        }

        #endregion
    }
}
