using System;
using Mogade;

namespace Flai.Mogade
{
    public class MogadePageLeaderboard : MogadeLeaderboard
    {
        private bool _needsRefresh = true;
        private int _scoresPerPage;
        private int _page = 1;

        public int ScoresPerPage
        {
            get { return _scoresPerPage; }
            set
            {
                if (_scoresPerPage > 2)
                {
                    _scoresPerPage = value;
                    _needsRefresh = true;
                }
            }
        }

        public int Page
        {
            get { return _page; }
            set
            {
                if (value > 2 && _page != value)
                {
                    _page = value;
                    _needsRefresh = true;
                }
            }
        }

        public bool NeedsRefresh
        {
            get { return _needsRefresh; }
        }

        // TODO: I'm not sure if CanLoadMoreScores -property of this leaderboard is working properly

        public MogadePageLeaderboard(IMogadeManager mogadeManager, string leaderboardId)
            : base(mogadeManager, leaderboardId)
        {
        }

        public void Refresh()
        {
            this.Refresh(null);
        }

        public void Refresh(Action<Response<LeaderboardScores>> userCallback)
        {
            if (_needsRefresh && !_isLoading && _canLoadMoreScores)
            {
                _needsRefresh = false;

                if (_page == 1)
                {
                    _canLoadMoreScores = true;
                }

                LeaderboardRequest request = new LeaderboardRequest(_scope, _page, _scoresPerPage);
                _latestRequest = request;
                Action<Response<LeaderboardScores>> callback = (response) =>
                    {
                        this.RefreshCallback(response, request);
                        if (userCallback != null)
                        {
                            userCallback(response);
                        }
                    };

                _mogadeManager.GetLeaderboard(_leaderboardId, _scope, _page, _scoresPerPage, callback);
                _isLoading = true;
            }
        }

        private void RefreshCallback(Response<LeaderboardScores> response, LeaderboardRequest request)
        {
            if (!request.Equals(_latestRequest))
            {
                return;
            }

            _isLoading = false;
            if (response.Success)
            {
                _scores.Clear();
                for (int i = 0; i < response.Data.Scores.Count; i++)
                {
                    _scores.Add(response.Data.Scores[i]);
                }

                if (response.Data.Scores.Count < request.Records)
                {
                    _canLoadMoreScores = false;
                }
            }
        }

        protected override void OnPropertyChanged(string changedPropertyName)
        {
            if (changedPropertyName == "Scope")
            {
                _needsRefresh = true;
            }
            base.OnPropertyChanged(changedPropertyName);
        }
    }
}
