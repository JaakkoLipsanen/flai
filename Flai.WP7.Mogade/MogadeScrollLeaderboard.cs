
using System;
using Mogade;

namespace Flai.Mogade
{
    public class MogadeScrollLeaderboard : MogadeLeaderboard
    {
        private int _scoresToLoad = 50;
        private int _page = 1;

        public int LastPage
        {
            get { return _page - 1; }
        }

        public int ScoresToLoad
        {
            get { return _scoresToLoad; }
            set
            {
                if (_scoresToLoad != value && value > 1)
                {
                    _scoresToLoad = value;
                    this.Reset();
                }
            }
        }

        public MogadeScrollLeaderboard(IMogadeManager mogadeManager, string leaderboardId)
            : base(mogadeManager, leaderboardId)
        {
        }

        public void LoadNextPage()
        {
            this.LoadNextPage(null);
        }

        public void LoadNextPage(Action<Response<LeaderboardScores>> userCallback)
        {
            if (!_isLoading && _canLoadMoreScores)
            {
                LeaderboardRequest request = new LeaderboardRequest(_scope, _page, _scoresToLoad); ;
                _latestRequest = request;
                Action<Response<LeaderboardScores>> callback = (response) =>
                {
                    this.LoadPageCallback(response, request);
                    if (userCallback != null)
                    {
                        userCallback(response);
                    }
                };

                _isLoading = true;
                _mogadeManager.GetLeaderboard(_leaderboardId, _scope, _page, _scoresToLoad, callback);
                _page++;
            }
        }


        private void LoadPageCallback(Response<LeaderboardScores> response, LeaderboardRequest request)
        {
            // If the leaderboard has been resetted ( thus, leaderboard is not loading ) or the response is not the response to the latest request,
            // do not proceed nor update data with this response
            if (!_isLoading || !request.Equals(_latestRequest))
            {
                return;
            }

            _isLoading = false;
            if (response.Success)
            {
                _scores.Capacity = Math.Max(_scores.Capacity, _scores.Count + response.Data.Scores.Count);
                for (int i = 0; i < response.Data.Scores.Count; i++)
                {
                    _scores.Add(response.Data.Scores[i]);
                }

                if (response.Data.Scores.Count < request.Records)
                {
                    _canLoadMoreScores = false;
                }
            }
        }

        protected override void OnPropertyChanged(string changedPropertyName)
        {
            if (changedPropertyName == "Scope")
            {
                this.Reset();
            }
            base.OnPropertyChanged(changedPropertyName);
        }

        private void Reset()
        {
            _scores.Clear();
            _page = 1;
            _isLoading = false;
            _canLoadMoreScores = true;
        }
    }
}
