﻿using System;
using System.Collections.Generic;

namespace Flai.StoreLauncher
{
    public interface IStore
    {
        ILicenseInformation LicenseInformation { get; }
        Guid AppId { get; }

        IAsyncOperationBase<IListingInformation> LoadListingInformationAsync();
        IAsyncOperationBase<string> RequestProductPurchaseAsync(string productId, bool includeReceipt);
        void ReportProductFulfillment(string productId);  
    }

    public interface ILicenseInformation
    {
        Dictionary<string, IProductLicense> ProductLicenses { get; }
    }

    // "Base" is stupid but possibly needed since WP8 has IAsyncOperation<T>
    public interface IAsyncOperationBase<T>
    {
        Action<IAsyncOperationBase<T>, StoreAsyncStatus> Completed { set; }
        T GetResults();
    }

    public interface IListingInformation
    {
        Dictionary<string, IProductListing> ProductListings { get; }
    }

    public interface IProductLicense
    {
        bool IsConsumable { get; set; }
        bool IsActive { get; set; }
        string ProductId { get; set; }
    }

    public interface IProductListing
    {
        string FormattedPrice { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        List<string> Keywords { get; set; }
        string ProductId { get; set; }
        StoreProductType ProductType { get; set; }
        string Tag { get; set; }
        Uri ImageUri { get; set; }
    }

    public enum StoreAsyncStatus
    {
        // Summary:
        //     The operation has started.
        Started = 0,
        //
        // Summary:
        //     The operation has completed.
        Completed = 1,
        //
        // Summary:
        //     The operation was canceled.
        Canceled = 2,
        //
        // Summary:
        //     The operation has encountered an error.
        Error = 3,
    }

    public enum StoreProductType
    {
        // Summary:
        //     The product type is unknown.
        Unknown = 0,
        //
        // Summary:
        //     A durable product.
        Durable = 1,
        //
        // Summary:
        //     A consumable product.
        Consumable = 2,
    }
}
