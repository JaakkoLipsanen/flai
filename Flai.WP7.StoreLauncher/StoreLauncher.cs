﻿using System;

namespace Flai.StoreLauncher
{
    public static class StoreLauncher
    {
        public static IStore GetStoreInterface()
        {
            return StoreLauncher.GetStoreInterface("Flai.StoreWrapper.Store, Flai.StoreWrapper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
        }

        public static IStore GetStoreInterface(string implementingType)
        {
            Type type = Type.GetType(implementingType);
            if (type != null)
            {
                return (IStore)Activator.CreateInstance(type);
            }

            return null;
        }
    }
}
