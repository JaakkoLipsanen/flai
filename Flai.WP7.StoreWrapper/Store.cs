﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Flai.StoreLauncher;

namespace Flai.StoreWrapper
{
    public class Store : IStore
    {
        public ILicenseInformation LicenseInformation
        {
            get { return new ApolloLicenseInformation(CurrentApp.LicenseInformation); }
        }

        public Guid AppId
        {
            get { return CurrentApp.AppId; }
        }

        public IAsyncOperationBase<IListingInformation> LoadListingInformationAsync()
        {
            return new ApolloIAsyncOperation<IListingInformation, ListingInformation>(CurrentApp.LoadListingInformationAsync(), r =>
                 {
                     r.Result = new ApolloListingInformation(r.ConvertFrom);
                 });
        }

        // no idea if this works
        public IAsyncOperationBase<string> RequestProductPurchaseAsync(string productId, bool includeReceipt)
        {
            return new ApolloIAsyncOperation<string, string>(CurrentApp.RequestProductPurchaseAsync(productId, includeReceipt), r =>
                {
                    r.Result = r.ConvertFrom;
                });
        }

        public void ReportProductFulfillment(string productId)
        {
            CurrentApp.ReportProductFulfillment(productId);
        }
    }

    #region IAsyncOperationConvertResult

    public class ApolloIAsyncOperationConvertResult<T, Y>
    {
        public Y ConvertFrom { get; set; }
        public T Result { get; set; }
    }

    #endregion

    #region IAsyncOperation

    public class ApolloIAsyncOperation<T, Y> : IAsyncOperationBase<T>
    {
        private readonly IAsyncOperation<Y> _result;
        private readonly Action<ApolloIAsyncOperationConvertResult<T, Y>> _converter;

        public ApolloIAsyncOperation(IAsyncOperation<Y> result, Action<ApolloIAsyncOperationConvertResult<T, Y>> converter)
        {
            _result = result;
            _converter = converter;
        }

        public T GetResults()
        {
            Y convertFrom = _result.GetResults();

            var convertResult = new ApolloIAsyncOperationConvertResult<T, Y>() { ConvertFrom = convertFrom };
            _converter(convertResult);

            return convertResult.Result;
        }

        public Action<IAsyncOperationBase<T>, StoreAsyncStatus> Completed
        {
            set
            {
                if (value != null)
                {
                    _result.Completed = (o, s) => value(this, (StoreAsyncStatus)s);
                }
            }
        }
    }

    #endregion

    #region License Information

    public class ApolloLicenseInformation : ILicenseInformation
    {
        private readonly LicenseInformation _licenseInformation;
        public ApolloLicenseInformation(LicenseInformation licenseInformation)
        {
            _licenseInformation = licenseInformation;
        }

        public Dictionary<string, IProductLicense> ProductLicenses
        {
            get
            {
                var productLicenses = new Dictionary<string, IProductLicense>();
                foreach (var kvp in _licenseInformation.ProductLicenses)
                {
                    productLicenses.Add(kvp.Key, new ApolloProductLicense(kvp.Value));
                }

                return productLicenses;
            }
        }
    }

    #endregion

    #region Listing Information

    public class ApolloListingInformation : IListingInformation
    {
        private readonly ListingInformation _listingInformation;
        public ApolloListingInformation(ListingInformation listingInformation)
        {
            _listingInformation = listingInformation;
        }

        public Dictionary<string, IProductListing> ProductListings
        {
            get
            {
                var productListings = new Dictionary<string, IProductListing>();
                foreach (var kvp in _listingInformation.ProductListings)
                {
                    productListings.Add(kvp.Key, new ApolloProductListing(kvp.Value));
                }

                return productListings;
            }
        }
    }

    #endregion

    #region Product License

    public class ApolloProductLicense : IProductLicense
    {
        private readonly ProductLicense _productLicense;
        public ApolloProductLicense(ProductLicense productLicense)
        {
            _productLicense = productLicense;
        }

        public bool IsConsumable
        {
            get { return _productLicense.IsConsumable; }
            set { }
        }

        public bool IsActive
        {
            get { return _productLicense.IsActive; }
            set { }
        }

        public string ProductId
        {
            get { return _productLicense.ProductId; }
            set { }
        }
    }

    #endregion

    #region Product Listing

    public class ApolloProductListing : IProductListing
    {
        private readonly ProductListing _productListing;
        public ApolloProductListing(ProductListing productListing)
        {
            _productListing = productListing;
        }

        public string FormattedPrice
        {
            get { return _productListing.FormattedPrice; }
            set { }
        }

        public string Name
        {
            get { return _productListing.Name; }
            set { }
        }

        public string Description
        {
            get { return _productListing.Description; }
            set { }
        }

        public List<string> Keywords
        {
            get { return new List<string>(_productListing.Keywords); }
            set { }
        }

        public string ProductId
        {
            get { return _productListing.ProductId; }
            set { }
        }

        public StoreProductType ProductType
        {
            get { return (StoreProductType)_productListing.ProductType; }
            set { }
        }

        public string Tag
        {
            get { return _productListing.Tag; }
            set { }
        }

        public Uri ImageUri
        {
            get { return _productListing.ImageUri; }
            set { }
        }
    }

    #endregion
}
