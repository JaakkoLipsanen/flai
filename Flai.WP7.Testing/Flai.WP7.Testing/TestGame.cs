
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Flai.WP7.Testing
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TestGame : FlaiGame
    {
        public TestGame()
        {
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
        }

        protected override void DrawInner(Graphics.GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, new Rectangle(20, 20, 100, 100), Color.Black);
            graphicsContext.SpriteBatch.End();
        }

        protected override void InitializeGraphicsSettings()
        {
            _graphicsDeviceManager.IsFullScreen = true;
            _graphicsDeviceManager.PreferMultiSampling = false;
            _graphicsDeviceManager.PreferredBackBufferFormat = SurfaceFormat.Color;
            _graphicsDeviceManager.PreferredBackBufferWidth = 800;
            _graphicsDeviceManager.PreferredBackBufferHeight = 480;
        }
    }
}
