using Flai.DataStructures;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using Flai.IO;

namespace Flai.Achievements
{
    public class AchievementManager
    {
        public event AchievementUnlockedDelegate AchievementUnlocked;

        private readonly string _achievementSaveFileName;
        private readonly Dictionary<string, Achievement> _achievementsByName = new Dictionary<string, Achievement>();
        private readonly Dictionary<string, AchievementGroup> _achievementGroupsByName = new Dictionary<string, AchievementGroup>(); 
        private readonly Dictionary<Achievement, AchievementGroup> _groupsByAchievement = new Dictionary<Achievement, AchievementGroup>(); 
        private readonly ReadOnlyArray<Achievement> _achievements;

        public ReadOnlyArray<Achievement> Achievements
        {
            get { return _achievements; }
        }

        // todo: UnlockedAchievements

        public AchievementManager(IEnumerable<Achievement> achievements, string achievementSaveFileName)
        {
            Ensure.NotNull(achievements);
            Ensure.NotNullOrWhitespace(achievementSaveFileName);
            Ensure.IsValidPath(achievementSaveFileName);

            _achievements = new ReadOnlyArray<Achievement>(achievements.ToArray());
            _achievementsByName = _achievements.ToDictionary(achivement => achivement.Name);
            _achievementSaveFileName = achievementSaveFileName;

            this.LoadFromFile();
            this.HookAchievementUnlockedEvents();
        }

        public Achievement this[string name]
        {
            get { return _achievementsByName[name]; }
        }

        // meh. i mean really meh, there's no guarantee that achievement in the group contains only achievements that are in this manager. but what-fucking-ever
        public void AddGroup(string name, AchievementGroup achievementGroup)
        {
            _achievementGroupsByName.Add(name, achievementGroup);
            for (int i = 0; i < achievementGroup.Achievements.Count; i++)
            {
                _groupsByAchievement.Add(achievementGroup.Achievements[i], achievementGroup);
            }
        }

        public AchievementGroup GetGroup(string name)
        {
            return _achievementGroupsByName[name];
        }

        public bool IsPartOfAnyGroup(Achievement achievement)
        {
            return _groupsByAchievement.ContainsKey(achievement);
        }

        public AchievementGroup GetGroupOf(Achievement achievement)
        {
            return _groupsByAchievement[achievement];
        }

        // oh my fucking god this is awful name. basically get's all achievements, but if the achievement is part of any group, then return it only if
        // it is "the latest" (aka, either if all achievements are unlocked then the last one, or else it is the next one to be unlocked)
        // todo: this is generates a ton of garbage
        public Achievement[] GetAchievementsWithoutGroups()
        {
            // ..... cache these collections? no need for thread safety, it isnt anyway
            List<Achievement> achievements = new List<Achievement>();
            for (int i = 0; i < _achievements.Length; i++)
            {
                Achievement achievement = _achievements[i];
                if (!this.IsPartOfAnyGroup(achievement)) // not part of any group
                {
                    achievements.Add(achievement);
                    continue;
                }

                AchievementGroup group = _groupsByAchievement[achievement];
                if (group.CurrentAchievement == achievement) // add to returned values only if the group isn't already contained
                {
                    achievements.Add(achievement);
                }
            }

            return achievements.ToArray();
        }

        #region Save/Load
        public void SaveToFile()
        {
            using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (BinaryWriter writer = new BinaryWriter(isolatedStorage.OpenFile(_achievementSaveFileName, FileMode.Create)))
                {
                    this.SaveInner(writer);
                }
            }
        }

        private void LoadFromFile()
        {
            using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isolatedStorage.FileExists(_achievementSaveFileName))
                {
                    using (BinaryReader reader = new BinaryReader(isolatedStorage.OpenFile(_achievementSaveFileName, FileMode.Open)))
                    {
                        this.LoadInner(reader);
                    }
                }
            }
        }

        private void SaveInner(BinaryWriter writer)
        {
            writer.Write(_achievements.Count);
            for (int i = 0; i < _achievements.Length; i++)
            {
                writer.WriteString(_achievements[i].Name);
                writer.Write(_achievements[i].Progression);

                object tag = _achievements[i].Tag;
                if (tag is IBinarySerializable) // ... i guess this is ok
                {
                    writer.Write(tag.Cast<IBinarySerializable>());
                }
            }
        }

        private void LoadInner(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                string name = reader.ReadString();
                reader.Read(this[name].Progression);

                object tag = _achievements[i].Tag;
                if (tag is IBinarySerializable) // ... i guess this is ok
                {
                    reader.Read(tag.Cast<IBinarySerializable>());
                }
            }
        }

        #endregion

        private void HookAchievementUnlockedEvents()
        {
            foreach (Achievement achievement in _achievements)
            {
                Achievement cachedAchievement = achievement;
                achievement.Unlocked += () => this.OnAchievementUnlocked(cachedAchievement);
            }
        }

        private void OnAchievementUnlocked(Achievement achievement)
        {
            this.AchievementUnlocked.InvokeIfNotNull(achievement);
        }
    }
}
