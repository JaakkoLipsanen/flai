
using System.Text;
using Flai.Graphics;
using Flai.Misc;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;

namespace Flai.Content
{
    // okay, this is NOT thread-safe anymore, because of the string builder. not sure if was even before, since dunno whats going on in the base ContentManager
    // also, this got a lot more messier because of the stringbuilder stuff, but its worth it.
    public class FlaiContentManager : ContentManager
    {
        private readonly Dictionary<StorageKey, object> _loadedAssets = new Dictionary<StorageKey, object>();
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        public const string FontFolder = "Fonts/";
        public const string TextureFolder = "Textures/";
        public const string SpriteSheetFolder = "SpriteSheets/";
        public const string EffectFolder = "Effects/";
        public const string ModelFolder = "Models/";
        public const string SoundEffectFolder = "Audio/SoundEffects/";

        public FlaiContentManager(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        public FlaiContentManager(IServiceProvider serviceProvider, string rootDirectory)
            : base(serviceProvider, rootDirectory)
        {
        }

        public override T Load<T>(string assetName)
        {
            return this.Load<T>(_stringBuilder.Append(assetName));
        }

        private T Load<T>(StringBuilder assetName)
        {
            // this is just a speed-up optimization. base.Load<T> does all kinds of checks etc which can be slow
            StorageKey key = new StorageKey(assetName);
            object value;
            if (_loadedAssets.TryGetValue(key, out value))
            {
                assetName.Clear();
                return (T)value;
            }

            string str = assetName.ToString();
            assetName.Clear();

            T asset = base.Load<T>(str);
            _loadedAssets.Add(new StorageKey(new StringBuilder(str)), asset);

            return asset;
        }

        public override void Unload()
        {
            _loadedAssets.Clear();
        }

        /// <summary>
        /// Loads font from content project.
        /// </summary>
        /// <param name="fontName"> Name of the font. </param>
        public SpriteFont LoadFont(string fontName)
        {
            return this.Load<SpriteFont>(_stringBuilder.Append(FlaiContentManager.FontFolder).Append(fontName));
        }

        /// <summary>
        /// Loads sound effect from content project
        /// </summary>
        public SoundEffect LoadSoundEffect(string soundEffectName)
        {
            return this.Load<SoundEffect>(_stringBuilder.Append(FlaiContentManager.SoundEffectFolder).Append(soundEffectName));
        }

        /// <summary>
        /// Loads texture from the content project.
        /// </summary>
        public Texture2D LoadTexture(string textureName)
        {
            return this.Load<Texture2D>(_stringBuilder.Append(FlaiContentManager.TextureFolder).Append(textureName));
        }

        public TextureDefinition LoadTextureFromSpriteSheet(string spriteSheetName, string textureName)
        {
            SpriteSheet spriteSheet = this.LoadSpriteSheet(spriteSheetName);
            return new TextureDefinition(spriteSheet.Texture, spriteSheet.GetSourceRectangle(textureName));
        }

        /// <summary>
        /// Loads sprite sheet from the content project.
        /// </summary>
        public SpriteSheet LoadSpriteSheet(string spriteSheetName)
        {
            return this.Load<SpriteSheet>(_stringBuilder.Append(FlaiContentManager.SpriteSheetFolder).Append(spriteSheetName));
        }

        /// <summary>
        /// Loads effect from the content project
        /// </summary>
        public Effect LoadEffect(string effectName)
        {
            return this.Load<Effect>(_stringBuilder.Append(FlaiContentManager.EffectFolder).Append(effectName));
        }

        /// <summary>
        /// Loads effect from the content project
        /// </summary>
        public Model LoadModel(string modelName)
        {
            return this.Load<Model>(_stringBuilder.Append(FlaiContentManager.ModelFolder).Append(modelName));
        }

        public bool AssetExists(string assetPath)
        {
            return File.Exists(Path.Combine(base.RootDirectory, assetPath + ".xnb"));
        }

        #region Storage Key

        // awful name
        // must use this since using StringBuilder as a key in dictionary doesn't work!
        private struct StorageKey : IEquatable<StorageKey>
        {
            private readonly StringBuilder _stringBuilder;
            private readonly int _hash;

            public StorageKey(StringBuilder stringBuilder)
            {
                _stringBuilder = stringBuilder;
                _hash = HashGenerator.GenerateHash(_stringBuilder);
            }

            public override int GetHashCode()
            {
                return _hash;
            }

            public bool Equals(StorageKey other)
            {
                if (_stringBuilder.Length != other._stringBuilder.Length)
                {
                    return false;
                }

                for (int i = 0; i < _stringBuilder.Length; i++)
                {
                    if (_stringBuilder[i] != other._stringBuilder[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            public override bool Equals(object obj)
            {
                return obj is StorageKey && this.Equals((StorageKey)obj);
            }

            public override string ToString()
            {
                return _stringBuilder.ToString();
            }
        }

        #endregion
    }
}
