﻿
namespace Flai.Content
{
    public interface IContentProvider
    {
        FlaiContentManager DefaultManager { get; }

        FlaiContentManager AddManager(string managerName);
        FlaiContentManager AddManager(string managerName, FlaiContentManager contentManager);

        FlaiContentManager GetManager(string managerName);
        bool RemoveManager(string managerName);

        FlaiContentManager this[string managerName] { get; }
    }
}
