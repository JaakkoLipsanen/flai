﻿
namespace Flai.General
{
    // should this be at Flai.DataStructures?
    public static class ArrayHelper<T>
    {
        public static readonly T[] Empty = new T[0];
    }
}
