
namespace Flai.Graphics
{
    // nothing *has to* follow these settings, but from now on all common things will follow these (if I remember this class when implementing said things :P).
    // not the best solution by any means since it's a bit difficult to use for non-debug etc stuff but blahh
    public static class GraphicalGuidelines
    {
        private static int _decimalPrecisionInText = 3;
        public static int DecimalPrecisionInText
        {
            get { return _decimalPrecisionInText; }
            set
            {
                Ensure.True(value >= 0);
                _decimalPrecisionInText = value;
            }
        }
    }
}
