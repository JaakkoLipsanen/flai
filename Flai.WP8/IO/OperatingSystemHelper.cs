using System;

namespace Flai.IO
{
    public static class OperatingSystemHelper
    {
        private static OperatingSystem _operatingSystem;
        public static OperatingSystem OperatingSystem
        {
            get { return _operatingSystem ?? (_operatingSystem = Environment.OSVersion); }
        }
    }
}
