using System;
using Flai.IO;
using System.IO;
using System.IO.IsolatedStorage;

namespace Flai.Misc
{
    // don't use "Settings" since if you have a "Settings" namespace in the game,
    // it'd make this class and the namespace to be.. ambiguous? from each other
    public abstract class SettingsBase : IBinarySerializable
    {
        public bool IsFirstLaunch { get; internal set; }

        void IBinarySerializable.Write(BinaryWriter writer)
        {
            this.WriteInner(writer);
        }

        void IBinarySerializable.Read(BinaryReader reader)
        {
            this.ReadInner(reader);
        }

        protected abstract void WriteInner(BinaryWriter writer);
        protected abstract void ReadInner(BinaryReader reader);
    }

    public class SettingsManager<T>
        where T : SettingsBase, new()
    {
        private readonly string _filePath;
        public T Settings { get; private set; }

        public SettingsManager(string filePath)
        {
            Ensure.IsValidPath(filePath);
            _filePath = filePath;

            this.LoadFromFile();
        }

        public void SaveToFile()
        {
#if WINDOWS_PHONE
            try
            {
                using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = isolatedStorage.OpenFile(_filePath, FileMode.Create, FileAccess.Write))
                    {
                        using (BinaryWriter writer = new BinaryWriter(stream))
                        {
                            writer.Write(this.Settings);
                        }
                    }
                }
            }
            catch { }
#else
            throw new NotImplementedException("");
#endif
        }

        private void LoadFromFile()
        {
            try
            {
#if WINDOWS_PHONE
                using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // could be not necessary
                    if (isolatedStorage.FileExists(_filePath))
                    {
                        using (IsolatedStorageFileStream stream = isolatedStorage.OpenFile(_filePath, FileMode.Open, FileAccess.Read))
                        {
                            using (BinaryReader reader = new BinaryReader(stream))
                            {
                                this.Settings = reader.ReadGeneric<T>();
                            }
                        }
                    }
                }
#else
                if (File.Exists(_filePath))
                {
                    using (var stream = File.OpenRead(_filePath))
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            this.Settings = reader.ReadGeneric<T>();
                        }
                    }
                }
#endif
            }
            catch { }


            if (this.Settings == null)
            {
                this.Settings = new T();
            }
        }
    }
}
