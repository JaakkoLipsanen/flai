
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace Flai.Ui
{
    public class TexturedButton : ButtonBase
    {
        public Sprite Sprite { get; private set; }

        public TexturedButton(Vector2 centerPosition, Sprite sprite)
            : this(new RectangleF(centerPosition.X - sprite.Texture.Width / 2f * sprite.Scale.X, centerPosition.Y - sprite.Texture.Height / 2f * sprite.Scale.Y, sprite.Texture.Width * sprite.Scale.Y, sprite.Texture.Height * sprite.Scale.Y), sprite)
        {
        }

        public TexturedButton(RectangleF area, Sprite sprite)
            : base(area)
        {
            this.Sprite = sprite;
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.Draw(this.Sprite, this.Area.ToRectangle());
        }
    }
}
