using System;
using System.Diagnostics;
using System.IO;
using Flai.CBES;
using Flai.CBES.Components;
using Flai.Graphics;
using Flai.Input;
using Flai.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Flai.Windows.Testing
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TestGame : FlaiGame
    {
        private Vector2 _position;
        private bool? _s;
        public TestGame()
        {
            CCamera2D.Active = new CCamera2D();
            Cursor.IsVisible = true;
        }

        protected override void LoadContentInner()
        {

        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _s = null;
            if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                _s = true;
            }
            else if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Right))
            {
                _s = false;
            }

            _position = updateContext.InputState.MousePosition;

            if (updateContext.InputState.IsKeyPressed(Keys.A))
            {
                CCamera2D.Active.Position -= Vector2.UnitX * 10;
            }
            else if (updateContext.InputState.IsKeyPressed(Keys.D))
            {
                CCamera2D.Active.Position += Vector2.UnitX * 10;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.W))
            {
                CCamera2D.Active.Position -= Vector2.UnitY * 10;
            }
            else if (updateContext.InputState.IsKeyPressed(Keys.S))
            {
                CCamera2D.Active.Position += Vector2.UnitY * 10;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.Up))
            {
                CCamera2D.Active.Zoom *= 0.99f;
            }
            else if (updateContext.InputState.IsKeyPressed(Keys.Down))
            {
                CCamera2D.Active.Zoom *= 1.01f;
            }
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.Begin(CCamera2D.Active);
            if (_s.HasValue)
            {
                if (_s.Value)
                {
                    graphicsContext.PrimitiveRenderer.DrawRectangle(RectangleF.CreateCentered(_position, 16), Color.Red);
                }
                else
                {
                    graphicsContext.PrimitiveRenderer.DrawRectangle(RectangleF.CreateCentered(CCamera2D.Active.ScreenToWorld(graphicsContext.ScreenSize, CCamera2D.Active.WorldToScreen(graphicsContext.ScreenSize, _position)), 16), Color.Red);
                }
            }

            graphicsContext.SpriteBatch.End();
        }

        protected override void InitializeGraphicsSettings()
        {
            _graphicsDeviceManager.IsFullScreen = false;
            _graphicsDeviceManager.PreferMultiSampling = false;
            _graphicsDeviceManager.PreferredBackBufferFormat = SurfaceFormat.Color;
            _graphicsDeviceManager.PreferredBackBufferWidth = 1920;
            _graphicsDeviceManager.PreferredBackBufferHeight = 1080;
        }
    }

    #region Entry Point

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (var game = new TestGame())
            {
                game.Run();
            }
        }
    }

    #endregion
}
