
using System.Collections;
using System.Collections.Generic;

namespace Flai.Content
{
    public class ContentProvider : FlaiService, IContentProvider, IEnumerable<FlaiContentManager>
    {
        private readonly Dictionary<string, FlaiContentManager> _contentManagers = new Dictionary<string, FlaiContentManager>();
       
        private string _rootDirectory = "Content";
        public string RootDirectory
        {
            get { return _rootDirectory; }
            set
            {
                if (_rootDirectory != value && !string.IsNullOrEmpty(value.Trim()))
                {
                    _rootDirectory = value.Trim();
                    foreach (FlaiContentManager contentManager in _contentManagers.Values)
                    {
                        contentManager.RootDirectory = _rootDirectory;
                    }
                }
            }
        }

        public FlaiContentManager DefaultManager
        {
            get { return this["Default"]; }
        }

        internal ContentProvider(FlaiServiceContainer services)
            : base(services)
        {
            _services.Add<IContentProvider>(this);
        }

        public FlaiContentManager AddManager(string managerName)
        {
            return this.AddManager(managerName, new FlaiContentManager(_services, _rootDirectory));
        }

        public FlaiContentManager AddManager(string managerName, FlaiContentManager contentManager)
        {
            if (!_contentManagers.ContainsKey(managerName))
            {
                _contentManagers.Add(managerName, contentManager);
            }
            return contentManager;
        }

        public FlaiContentManager GetManager(string managerName)
        {
            return this[managerName];
        }

        public bool UnloadManager(string managerName)
        {
            FlaiContentManager contentManager;
            if (_contentManagers.TryGetValue(managerName, out contentManager))
            {
                contentManager.Unload();
                return true;
            }

            return false;
        }

        public bool RemoveManager(string managerName)
        {
            return _contentManagers.Remove(managerName);
        }

        public FlaiContentManager this[string managerName]
        {
            get
            {
                FlaiContentManager value;
                if (!_contentManagers.TryGetValue(managerName, out value))
                {
                    value = this.AddManager(managerName);
                }

                return value;
            }
        }

        #region IEnumerable<FlaiContentManager> Members

        public IEnumerator<FlaiContentManager> GetEnumerator()
        {
            return _contentManagers.Values.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _contentManagers.Values.GetEnumerator();
        }

        #endregion
    }
}
