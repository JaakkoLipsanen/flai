﻿using System.Collections.Generic;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flai.Content.Readers
{
    internal class SpriteSheetReader : ContentTypeReader<SpriteSheet>
    {
        protected override SpriteSheet Read(ContentReader input, SpriteSheet existingInstance)
        {
            FlaiContentManager contentManager = input.ContentManager as FlaiContentManager;

            Texture2D texture;
            if (contentManager != null)
            {
                texture = contentManager.LoadTexture(input.ReadString());
            }
            else
            {
                texture = input.ContentManager.Load<Texture2D>("Textures/" + input.ReadString());
            }

            int spriteCount = input.ReadInt32();
            Dictionary<string, Rectangle> sprites = new Dictionary<string, Rectangle>(spriteCount);
            for (int i = 0; i < spriteCount; i++)
            {
                string spriteName = input.ReadString();
                Rectangle sourceArea = new Rectangle(
                    input.ReadInt32(),
                    input.ReadInt32(),
                    input.ReadInt32(),
                    input.ReadInt32());

                sprites.Add(spriteName, sourceArea);
            }

            return new SpriteSheet(texture, sprites);
        }
    }
}
