﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace Flai.Graphics
{
    public enum CompiledEffect
    {
        CircleEffect
    }

    public static class CompiledEffects
    {
        public static byte[] GetEffectBytes(CompiledEffect effect)
        {
            return CompiledEffects.GetEffectBytes(effect, FlaiGame.Current.GraphicsDeviceManager.GraphicsProfile);
        }

        public static byte[] GetEffectBytes(CompiledEffect effect, GraphicsProfile graphicsProfile)
        {
            switch (effect)
            {
                case CompiledEffect.CircleEffect:
                    return graphicsProfile == GraphicsProfile.HiDef ? CircleEffect.HiDefWindows : CircleEffect.ReachWindows;

                default:
                    throw new Exception("Effect not found");  
            }
        }

        #region Circle Effect

        private static class CircleEffect
        {
            #region HiDef Windows

            public static byte[] HiDefWindows = 
            {
	207, 11, 240, 188, 16, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	1, 9, 255, 254, 8, 1, 0, 0, 
	0, 0, 0, 0, 3, 0, 0, 0, 
	1, 0, 0, 0, 48, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	4, 0, 0, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 128, 62, 0, 0, 64, 63, 
	10, 0, 0, 0, 70, 105, 108, 108, 
	67, 111, 108, 111, 114, 0, 0, 0, 
	3, 0, 0, 0, 0, 0, 0, 0, 
	96, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 0, 0, 
	1, 0, 0, 0, 10, 215, 35, 60, 
	11, 0, 0, 0, 83, 116, 114, 111, 
	107, 101, 83, 105, 122, 101, 0, 0, 
	3, 0, 0, 0, 1, 0, 0, 0, 
	156, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 4, 0, 0, 0, 
	1, 0, 0, 0, 0, 0, 128, 63, 
	0, 0, 128, 63, 0, 0, 128, 63, 
	0, 0, 128, 63, 12, 0, 0, 0, 
	83, 116, 114, 111, 107, 101, 67, 111, 
	108, 111, 114, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 204, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	1, 0, 0, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 13, 0, 0, 0, 
	83, 109, 111, 111, 116, 104, 83, 116, 
	114, 111, 107, 101, 0, 0, 0, 0, 
	1, 0, 0, 0, 15, 0, 0, 0, 
	4, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	3, 0, 0, 0, 80, 48, 0, 0, 
	1, 0, 0, 0, 0, 0, 0, 0, 
	4, 0, 0, 0, 1, 0, 0, 0, 
	2, 0, 0, 0, 2, 0, 0, 0, 
	4, 0, 0, 0, 32, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	64, 0, 0, 0, 92, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	112, 0, 0, 0, 140, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	172, 0, 0, 0, 200, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 1, 0, 0, 0, 0, 0, 0, 
	1, 0, 0, 0, 248, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 0, 0, 
	147, 0, 0, 0, 0, 0, 0, 0, 
	228, 0, 0, 0, 224, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	255, 255, 255, 255, 0, 0, 0, 0, 
	0, 0, 0, 0, 40, 5, 0, 0, 
	0, 2, 255, 255, 254, 255, 83, 0, 
	67, 84, 65, 66, 28, 0, 0, 0, 
	23, 1, 0, 0, 0, 2, 255, 255, 
	4, 0, 0, 0, 28, 0, 0, 0, 
	0, 0, 0, 32, 16, 1, 0, 0, 
	108, 0, 0, 0, 2, 0, 2, 0, 
	1, 0, 0, 0, 120, 0, 0, 0, 
	136, 0, 0, 0, 152, 0, 0, 0, 
	2, 0, 5, 0, 1, 0, 0, 0, 
	168, 0, 0, 0, 184, 0, 0, 0, 
	200, 0, 0, 0, 2, 0, 4, 0, 
	1, 0, 0, 0, 120, 0, 0, 0, 
	212, 0, 0, 0, 228, 0, 0, 0, 
	2, 0, 3, 0, 1, 0, 0, 0, 
	240, 0, 0, 0, 0, 1, 0, 0, 
	70, 105, 108, 108, 67, 111, 108, 111, 
	114, 0, 171, 171, 1, 0, 3, 0, 
	1, 0, 4, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 128, 62, 
	0, 0, 64, 63, 83, 109, 111, 111, 
	116, 104, 83, 116, 114, 111, 107, 101, 
	0, 171, 171, 171, 0, 0, 1, 0, 
	1, 0, 1, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 83, 116, 114, 111, 
	107, 101, 67, 111, 108, 111, 114, 0, 
	0, 0, 128, 63, 0, 0, 128, 63, 
	0, 0, 128, 63, 0, 0, 128, 63, 
	83, 116, 114, 111, 107, 101, 83, 105, 
	122, 101, 0, 171, 0, 0, 3, 0, 
	1, 0, 1, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 10, 215, 35, 60, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 112, 115, 95, 50, 
	95, 48, 0, 77, 105, 99, 114, 111, 
	115, 111, 102, 116, 32, 40, 82, 41, 
	32, 72, 76, 83, 76, 32, 83, 104, 
	97, 100, 101, 114, 32, 67, 111, 109, 
	112, 105, 108, 101, 114, 32, 57, 46, 
	50, 54, 46, 57, 53, 50, 46, 50, 
	56, 52, 52, 0, 254, 255, 116, 0, 
	80, 82, 69, 83, 1, 2, 88, 70, 
	254, 255, 37, 0, 67, 84, 65, 66, 
	28, 0, 0, 0, 95, 0, 0, 0, 
	1, 2, 88, 70, 1, 0, 0, 0, 
	28, 0, 0, 0, 0, 1, 0, 32, 
	92, 0, 0, 0, 48, 0, 0, 0, 
	2, 0, 0, 0, 1, 0, 0, 0, 
	60, 0, 0, 0, 76, 0, 0, 0, 
	83, 116, 114, 111, 107, 101, 83, 105, 
	122, 101, 0, 171, 0, 0, 3, 0, 
	1, 0, 1, 0, 1, 0, 0, 0, 
	0, 0, 0, 0, 10, 215, 35, 60, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 116, 120, 0, 77, 
	105, 99, 114, 111, 115, 111, 102, 116, 
	32, 40, 82, 41, 32, 72, 76, 83, 
	76, 32, 83, 104, 97, 100, 101, 114, 
	32, 67, 111, 109, 112, 105, 108, 101, 
	114, 32, 57, 46, 50, 54, 46, 57, 
	53, 50, 46, 50, 56, 52, 52, 0, 
	254, 255, 12, 0, 80, 82, 83, 73, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 2, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	1, 0, 0, 0, 0, 0, 0, 0, 
	2, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 254, 255, 18, 0, 
	67, 76, 73, 84, 8, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 224, 63, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	254, 255, 42, 0, 70, 88, 76, 67, 
	4, 0, 0, 0, 1, 0, 80, 160, 
	2, 0, 0, 0, 0, 0, 0, 0, 
	2, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 0, 0, 
	4, 0, 0, 0, 0, 0, 0, 0, 
	7, 0, 0, 0, 0, 0, 0, 0, 
	1, 0, 16, 16, 1, 0, 0, 0, 
	0, 0, 0, 0, 7, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	7, 0, 0, 0, 4, 0, 0, 0, 
	1, 0, 64, 160, 2, 0, 0, 0, 
	0, 0, 0, 0, 7, 0, 0, 0, 
	4, 0, 0, 0, 0, 0, 0, 0, 
	1, 0, 0, 0, 4, 0, 0, 0, 
	0, 0, 0, 0, 4, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 48, 16, 
	1, 0, 0, 0, 0, 0, 0, 0, 
	2, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 4, 0, 0, 0, 
	4, 0, 0, 0, 240, 240, 240, 240, 
	15, 15, 15, 15, 255, 255, 0, 0, 
	81, 0, 0, 5, 6, 0, 15, 160, 
	0, 0, 0, 63, 0, 0, 0, 192, 
	0, 0, 128, 63, 0, 0, 0, 0, 
	31, 0, 0, 2, 0, 0, 0, 128, 
	0, 0, 3, 176, 2, 0, 0, 3, 
	0, 0, 8, 128, 0, 0, 85, 177, 
	6, 0, 0, 160, 35, 0, 0, 2, 
	0, 0, 1, 128, 0, 0, 255, 128, 
	5, 0, 0, 3, 0, 0, 1, 128, 
	0, 0, 0, 128, 0, 0, 0, 128, 
	2, 0, 0, 3, 0, 0, 2, 128, 
	0, 0, 0, 177, 6, 0, 0, 160, 
	35, 0, 0, 2, 0, 0, 2, 128, 
	0, 0, 85, 128, 4, 0, 0, 4, 
	0, 0, 1, 128, 0, 0, 85, 128, 
	0, 0, 85, 128, 0, 0, 0, 128, 
	7, 0, 0, 2, 0, 0, 1, 128, 
	0, 0, 0, 128, 6, 0, 0, 2, 
	0, 0, 1, 128, 0, 0, 0, 128, 
	2, 0, 0, 3, 0, 0, 2, 128, 
	0, 0, 0, 129, 0, 0, 0, 160, 
	35, 0, 0, 2, 0, 0, 2, 128, 
	0, 0, 85, 128, 5, 0, 0, 3, 
	0, 0, 2, 128, 0, 0, 85, 128, 
	1, 0, 0, 160, 4, 0, 0, 4, 
	0, 0, 2, 128, 0, 0, 85, 128, 
	6, 0, 85, 160, 6, 0, 170, 160, 
	4, 0, 0, 4, 0, 0, 4, 128, 
	0, 0, 85, 128, 0, 0, 85, 129, 
	6, 0, 170, 160, 5, 0, 0, 3, 
	1, 0, 8, 128, 0, 0, 85, 128, 
	0, 0, 85, 128, 5, 0, 0, 3, 
	1, 0, 7, 128, 1, 0, 255, 128, 
	4, 0, 228, 160, 4, 0, 0, 4, 
	2, 0, 7, 128, 2, 0, 228, 160, 
	0, 0, 170, 128, 1, 0, 228, 128, 
	2, 0, 0, 3, 0, 0, 2, 128, 
	0, 0, 0, 128, 0, 0, 0, 161, 
	1, 0, 0, 2, 2, 0, 8, 128, 
	6, 0, 170, 160, 88, 0, 0, 4, 
	1, 0, 15, 128, 0, 0, 85, 128, 
	1, 0, 228, 128, 2, 0, 228, 128, 
	1, 0, 0, 2, 2, 0, 15, 128, 
	4, 0, 228, 160, 88, 0, 0, 4, 
	1, 0, 15, 128, 5, 0, 0, 161, 
	2, 0, 228, 128, 1, 0, 228, 128, 
	2, 0, 0, 3, 0, 0, 2, 128, 
	0, 0, 0, 129, 6, 0, 0, 160, 
	2, 0, 0, 3, 0, 0, 1, 128, 
	0, 0, 0, 129, 3, 0, 0, 161, 
	2, 0, 0, 3, 0, 0, 1, 128, 
	0, 0, 0, 128, 6, 0, 0, 160, 
	88, 0, 0, 4, 0, 0, 1, 128, 
	0, 0, 0, 128, 6, 0, 255, 161, 
	6, 0, 170, 161, 88, 0, 0, 4, 
	0, 0, 1, 128, 0, 0, 85, 128, 
	0, 0, 0, 128, 6, 0, 170, 161, 
	88, 0, 0, 4, 1, 0, 15, 128, 
	0, 0, 85, 128, 1, 0, 228, 128, 
	6, 0, 255, 160, 88, 0, 0, 4, 
	0, 0, 15, 128, 0, 0, 0, 128, 
	2, 0, 228, 160, 1, 0, 228, 128, 
	1, 0, 0, 2, 0, 8, 15, 128, 
	0, 0, 228, 128, 255, 255, 0, 0, 
};
            #endregion

            #region Reach Windows

            public static readonly byte[] ReachWindows = CircleEffect.HiDefWindows;

            #endregion
        }

        #endregion
    }
}
