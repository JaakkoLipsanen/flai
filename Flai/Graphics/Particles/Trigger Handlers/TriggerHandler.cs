
namespace Flai.Graphics.Particles.TriggerHandlers
{
    public abstract class TriggerHandler
    {
        protected internal abstract void Process(ref TriggerContext triggerContext);
    }
}
