﻿
namespace Flai.Pathfinding
{
    public interface IMap2D
    {
        bool IsFree(Vector2i position);
    }
}
