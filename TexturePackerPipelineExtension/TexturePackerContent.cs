﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework;

namespace TexturePackerContentPipeline
{
    public class TexturePackerContent
    {
        public class TexturePackerRegionContent
        {
            public readonly string TextureName;
            public readonly Rectangle Area;

            internal TexturePackerRegionContent(string textureName, Rectangle area)
            {
                this.TextureName = textureName;
                this.Area = area;
            }  
        }

        private readonly string _imagePath;
        private readonly List<TexturePackerRegionContent> _sprites;
        private readonly ReadOnlyCollection<TexturePackerRegionContent> _readOnlySprites;

        public string ImagePath
        {
            get { return _imagePath; }
        }

        public ReadOnlyCollection<TexturePackerRegionContent> Sprites
        {
            get { return _readOnlySprites; }
        }

        public TexturePackerContent(string imagePath, List<TexturePackerRegionContent> sprites)
        {
            _imagePath = imagePath;
            _sprites = sprites;

            _readOnlySprites = _sprites.AsReadOnly();
        }
    }
}
