using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using System.ComponentModel;

using TInput = TexturePackerContentPipeline.TexturePackerContent;
using TOutput = TexturePackerContentPipeline.TexturePackerContent;

namespace TexturePackerContentPipeline
{
    [ContentProcessor(DisplayName = "Texture Packer Processor")]
    public class TexturePackerProcessor : ContentProcessor<TInput, TOutput>
    {
        [DefaultValue(typeof(Color), "255, 0, 255, 255")]
        [DisplayName("Color Key Color")]
        [Description("If the texture is color-keyed, pixels of this color are replaced with transparent black")]
        public Color ColorKeyColor { get; set; }

        [DefaultValue(true)]
        [DisplayName("Color Key Enabled")]
        [Description("If enabled, the source of texture is color keyed. Pixels matching this value of \"Color Key Color\" are replaced with transparent black")]
        public bool ColorKeyEnabled { get; set; }

        [DefaultValue(true)]
        [DisplayName("Premltiply Alpha")]
        [Description("If enabled, the texture is converted to premulitplied alpha format")]
        public bool PremultiplyAlpha { get; set; }

        [DefaultValue(TextureProcessorOutputFormat.Color)]
        [DisplayName("Texture Format")]
        [Description("Specifies the SurfaceFormat type of processed textures. Textures can either remain unchanged from the source asset, converted to the Color Format, or DXT Compressed")]
        public TextureProcessorOutputFormat TextureFormat { get; set; }

        public override TOutput Process(TInput input, ContentProcessorContext context)
        {
            return input;
        }
    }
}