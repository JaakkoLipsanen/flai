using Flai.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

// TODO: replace this with the type you want to write out.
using TWrite = TexturePackerContentPipeline.TexturePackerContent;

namespace TexturePackerContentPipeline
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class TexturePackerWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            output.Write(value.ImagePath);
            output.Write(value.Sprites.Count);
            foreach (var val in value.Sprites)
            {
                output.Write(val.TextureName);

                output.Write(val.Area.X);
                output.Write(val.Area.Y);
                output.Write(val.Area.Width);
                output.Write(val.Area.Height);               
            }
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(SpriteSheet).AssemblyQualifiedName;
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            // TODO: change this to the name of your ContentTypeReader
            // class which will be used to load this data.
            return "Flai.Content.Readers.SpriteSheetReader, Flai, Version=1.0.0.0, Culture=neutral";
        }
    }
}
