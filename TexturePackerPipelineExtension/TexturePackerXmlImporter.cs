﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using TOutput = TexturePackerContentPipeline.TexturePackerContent;

namespace TexturePackerContentPipeline
{
    [ContentImporter(".xml", DefaultProcessor = "TexturePackerProcessor", DisplayName = "TexturePacker XML Importer")]
    public class TexturePackerXmlImporter : ContentImporter<TOutput>
    {
        public override TOutput Import(string filename, ContentImporterContext context)
        {
            string imagePath = "";
            List<TexturePackerContent.TexturePackerRegionContent> sprites = new List<TOutput.TexturePackerRegionContent>();

            var xDocument = XDocument.Parse(File.ReadAllText(filename));
            using (var reader = xDocument.CreateReader())
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "TextureAtlas")
                    {
                        imagePath = reader.GetAttribute("imagePath");
                    }
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "sprite")
                    {
                        if (reader.HasAttributes)
                        {
                            string spriteName = reader.GetAttribute("n");
                            if (spriteName.Contains("."))
                            {
                                spriteName = spriteName.Split('.')[0];
                            }

                            Rectangle spriteSourceArea = new Rectangle(
                                    int.Parse(reader.GetAttribute("x")),
                                    int.Parse(reader.GetAttribute("y")),
                                    int.Parse(reader.GetAttribute("w")),
                                    int.Parse(reader.GetAttribute("h")));

                            sprites.Add(new TOutput.TexturePackerRegionContent(spriteName, spriteSourceArea));
                        }
                    }
                }
            }

            imagePath = Path.GetFileNameWithoutExtension(imagePath);
            return new TOutput(imagePath, sprites);
        }
    }
}
